using System.Collections.Generic;
using Microsoft.Extensions.Options;
using parser.Models;
using System.Configuration;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Extensions.Configuration;
using System;

namespace parser.Services
{
    public static class XMLService
    {

        public static IConfigurationRoot Configuration { get; set; }

        private static XMLParser _parser = new XMLParser();
        private static XMLGenerator _generator = new XMLGenerator();
        
        public static void Init()
        {
            PHXDatabase.sql.ConnectionString = Configuration.GetSection("ConnectionStrings:PHXConnectionString").Value;
            XMLDatabase.sql.ConnectionString = Configuration.GetSection("ConnectionStrings:XMLConnectionString").Value;

            _parser.Init();
            _generator.Init();
        }

        public static void SetIncomingDirectory(string directory)
        {
            _parser.Init(directory);
        }

        public static void SetOutgoingDirectory(string directory)
        {
            _generator.Init(directory);
        }

        public static bool StartParser()
        {
            _parser.StartMonitoringDirectory();
            return true;
        }
        
        public static bool StopParser()
        {
            _parser.StopMonitoringDirectory();
            return true;
        }

        public static bool ParseFile(string filePath)
        {
            _parser.ResendFile(filePath);
            return true;
        }

        public static bool GenerateFile(object data)
        {
            _generator.GetData(data.ToString(), Models.OutgoingXML.OutgoingTypes.WorkOrderUpdate);
            
            return true;
        }
    }
}