﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using parser.Models.Log;

namespace parser.Models
{
    public class Database
    {
        public string ConnectionString { get; set; }

        public Database(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public DataTable ExecuteStoredProcedure(string procedureName)
        {
            DataTable dt = new DataTable();
            SqlConnection sqlConnObj = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand sqlCmd = new SqlCommand(procedureName, sqlConnObj)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlConnObj.Open();


                //dt.Load(sqlCmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                Logger.Log(ex.Message);
            }
            finally
            {
                sqlConnObj.Close();
            }

            return dt;
        }

        public DataTable ExecuteStoredProcedure(string procedureName, List<SqlParameter> parameters)
        {
            DataTable dt = new DataTable();
            SqlConnection sqlConnObj = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand sqlCmd = new SqlCommand(procedureName, sqlConnObj)
                {
                    CommandType = CommandType.StoredProcedure
                };

                foreach (var param in parameters)
                {
                    sqlCmd.Parameters.Add(param);
                }

                sqlConnObj.Open();
                //sqlCmd.ExecuteNonQuery();

                dt.Load(sqlCmd.ExecuteReader());

                sqlConnObj.Close();
            }
            catch (SqlException ex)
            {
                sqlConnObj.Close();
                Logger.Log(ex.Message);
            }
            return dt;
        }

        public DataTable ExcecuteTransactionQuery(String procedureName)
        {
            DataTable dt = new DataTable();
            SqlConnection sqlConnObj = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand sqlCmd = new SqlCommand(procedureName, sqlConnObj);
                //sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandType = CommandType.Text;

                sqlConnObj.Open();

                dt.Load(sqlCmd.ExecuteReader());

                sqlConnObj.Close();
            }
            catch (StackOverflowException ofEX)
            {
                sqlConnObj.Close();
                Logger.Log(ofEX.Message);
            }
            catch (SqlException ex)
            {
                sqlConnObj.Close();
                Logger.Log(ex.Message);
            }

            return dt;
        }

        public List<Dictionary<string, object>> ExecuteQueryToList(string query)
        {
            string results = "";
            SqlConnection sqlConnObj = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand sqlCmd = new SqlCommand(query, sqlConnObj);
                //sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandType = CommandType.Text;

                sqlConnObj.Open();

                DataTable dt = new DataTable();
                dt.Load(sqlCmd.ExecuteReader());

                return ResultsToList(dt);
            }
            catch (SqlException ex)
            {
                Logger.Log(ex.Message);
            }
            finally
            {
                sqlConnObj.Close();
            }

            return null;
        }

        public List<Dictionary<string, object>> ResultsToList(DataTable dt)
        {
            var list = new List<Dictionary<string, object>>();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        var dict = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            dict[col.ColumnName] = (Convert.ToString(row[col]));
                        }
                        list.Add(dict);
                    }
                }
            }

            return list;
        }
    }
}