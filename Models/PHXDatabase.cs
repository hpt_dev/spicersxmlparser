﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using parser.Models.IncomingXML;
using System.Diagnostics;
using parser.Models.Log;
using System.Reflection.Metadata.Ecma335;

namespace parser.Models
{
    public static class PHXDatabase
    {
        public static string ConnString = "";//"Data Source=25.95.248.242;Initial Catalog=phoenix_spicers;Persist Security Info=True;User ID=phoenix_user;Password=admin";
        //private static string ConnString = "Data Source=localhost;Initial Catalog=phoenix_test;Persist Security Info=True;User ID=phoenix_user;Password=admin";
        public static Database sql = new Database(ConnString);

        #region Stored Procedure names
        public const string ImportSalesOrder = "dbo.spPHX_Import_SalesOrder";
        public const string ImportWorkOrder = "dbo.spPHX_Import_WorkOrder";
        public const string ImportCustomer = "dbo.spPHX_Import_Customer";
        public const string ImportLocation = "dbo.spPHX_Import_Locations";
        public const string ImportBuilding = "dbo.spPHX_Import_Building";
        public const string ImportItem = "dbo.spPHX_Import_Item";
        public const string ImportBOM = "dbo.spPHX_Import_BOM";
        public const string ImportBOMStep = "dbo.spPHX_Import_BOMSteps";
        public const string ImportBOMProducedItem = "dbo.spPHX_Import_BOMProducedItem";
        public const string ImportBOMRequiredMaterial = "dbo.spPHX_Import_BOMRequiredMaterial";
        public const string ImportInventory = "dbo.spPHX_Import_Inventory";
        public const string ScheduleOrder = "dbo.spPHX_IntegrationAddWOBOMRouteSteps";
        public const string DeleteOrder = "dbo.so_CancelSalesOrder";
        public const string ExportWorkOrder = "dbo.spAP_ExportWorkOrder";
        public const string CombineCoproductOrders = "dbo.spPHX_IntegrationCombineCoproducts";
        //public const string UpdateSalesOrderCustomer = "dbo."
        #endregion

        public static ParseResults SyncWorkOrder(WorkOrder_Info workOrder, string fileName)
        {
            DataTable results = new DataTable();
            string validateResults = "";
            string syncType = "";

            try
            {
                syncType = workOrder.WoHdr_Rec.wo_status.ToUpper();

                switch (syncType)
                {
                    case "DELETE":
                        {
                            //Cancel order
                            DeleteWorkOrder(workOrder);
                            return new ParseResults("Deleted", true);
                        }
                    case "CUSTOMERCHANGE":
                        {
                            //Customer Should only be changing in this state
                            validateResults += ValidateBillToCustomer(workOrder.WoHdr_Rec.Bill_to_Customer);
                            validateResults += ValidateShipToCustomer(workOrder.WoHdr_Rec.Ship_to_Customer);

                            if (validateResults == "")
                            {
                                DataTable billToResults = new DataTable();
                                DataTable shipToResults = new DataTable();

                                billToResults = AddUpdateBillToCustomer(workOrder.WoHdr_Rec.Bill_to_Customer, workOrder.WoHdr_Rec.Ship_to_Customer);
                                shipToResults = AddUpdateShipToCustomer(workOrder.WoHdr_Rec.Ship_to_Customer, workOrder.WoHdr_Rec.Bill_to_Customer);

                                DataTable salesOrderResults = GetSalesOrder(workOrder.WoHdr_Rec.workorder);
                                DataTable workOrderResults = GetWorkOrder(workOrder.WoHdr_Rec.workorder);

                                DataTable soBillToResults = UpdateSalesOrderBillToCustomer(workOrder, billToResults, salesOrderResults);
                                DataTable soShipToResults = UpdateSalesOrderShipToCustomer(workOrder, shipToResults, salesOrderResults, workOrderResults);

                                return new ParseResults(validateResults, true);
                            }
                            else
                                return new ParseResults(validateResults, false);

                        }
                    case "CHANGE":
                    case "ADD":
                        {
                            //Full Import/Update
                            validateResults = ValidateWorkOrderData(workOrder);

                            if (validateResults == "")
                            {

                                DataTable billToResults = new DataTable();
                                DataTable shipToResults = new DataTable();
                                DataTable buildingResults = AddUpdateBuilding(workOrder.WoHdr_Rec.fg_warehouse);
                                DataTable finishItemResults = AddUpdateFinishItem(workOrder.WoHdr_Rec.Finish_item);

                                if (buildingResults.Rows.Count == 0)
                                {
                                    string msg = "Failed to Add Building";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }

                                if (workOrder.WoHdr_Rec.wo_type == "REPLENISHMENT")
                                {
                                    //Default Customers for Replenishment
                                    Bill_to_Customer defaultBillTo = GetDefaultBillCustomer();
                                    Ship_to_Customer defaultShipTo = GetDefaultShipCustomer();

                                    billToResults = AddUpdateBillToCustomer(defaultBillTo, defaultShipTo);
                                    shipToResults = AddUpdateShipToCustomer(defaultShipTo, defaultBillTo);
                                }
                                else
                                {
                                    billToResults = AddUpdateBillToCustomer(workOrder.WoHdr_Rec.Bill_to_Customer, workOrder.WoHdr_Rec.Ship_to_Customer);
                                    shipToResults = AddUpdateShipToCustomer(workOrder.WoHdr_Rec.Ship_to_Customer, workOrder.WoHdr_Rec.Bill_to_Customer);
                                }

                                if (billToResults.Rows.Count == 0) //Validate
                                {
                                    string msg = "Failed to Get/Add Bill To Customer";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }

                                if (shipToResults.Rows.Count == 0) //Validate
                                {
                                    string msg = "Failed to Get/Add Ship To Customer";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }

                                if (finishItemResults.Rows.Count == 0) //Validate
                                {
                                    string msg = "Failed to Get/Add Finish Item";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }

                                // Bill of Materials
                                DataTable bomResults = AddUpdateBillOfMaterials(workOrder.WoHdr_Rec.Finish_item, finishItemResults, workOrder.WoHdr_Rec.workorder);// workOrder.WoHdr_Rec.workorder);
                                if (bomResults.Rows.Count == 0) //Validate
                                {
                                    string msg = "Failed to Get/Add Bill of Material Template";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }

                                DataTable stepResults = AddUpdateBOMSteps(workOrder.WoHdr_Rec.Routing, workOrder.WoHdr_Rec.Finish_item, bomResults, workOrder.WoHdr_Rec.workorder);
                                if (stepResults.Rows.Count == 0) //Validate
                                {
                                    string msg = "Failed to Get/Add Bill of Material Step";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }

                                DataTable producedItemResults = AddUpdateBOMProducedItem(workOrder.WoHdr_Rec.Routing, workOrder.WoHdr_Rec.Finish_item, stepResults, finishItemResults, workOrder.WoHdr_Rec.workorder);
                                if (producedItemResults.Rows.Count == 0) //Validate
                                {
                                    string msg = "Failed to Get/Add Bill of Material Produced Item";
                                    Logger.Log(msg);
                                    return new ParseResults(msg, false);
                                }
                                
                                int multipleComponentsCount = 1;

                                foreach (Component component in workOrder.WoHdr_Rec.Components.Component)
                                {
                                    DataTable componentBuildingResults = AddUpdateComponentBuilding(component);

                                    if (componentBuildingResults.Rows.Count == 0)//Validate
                                    {
                                        string msg = "Failed to Get/Add Component Warehouse";
                                        Logger.Log(msg);
                                        return new ParseResults(msg, false);
                                    }

                                    DataTable componentItemResults = AddUpdateComponentItem(component);

                                    if (componentItemResults.Rows.Count == 0) //Validate
                                    {
                                        string msg = "Failed to Get/Add Component Item";
                                        Logger.Log(msg);
                                        return new ParseResults(msg, false);
                                    }

                                    DataTable requiredMaterialResults = AddUpdateBOMRequiredMaterials(component, workOrder.WoHdr_Rec.Routing, stepResults, componentItemResults, workOrder.WoHdr_Rec.workorder, multipleComponentsCount);
                                    if (requiredMaterialResults.Rows.Count == 0) //Validate
                                    {
                                        string msg = "Failed to Get/Add Bill of Material Required Materials";
                                        Logger.Log(msg);
                                        return new ParseResults(msg, false);
                                    }

                                    DataTable inventoryResults = AddUpdateOnHandInventory(component, componentItemResults, componentBuildingResults);

                                    if (inventoryResults.Rows.Count == 0) //Validate
                                    {
                                        string msg = "Failed to Get/Add Inventory";
                                        Logger.Log(msg);
                                        return new ParseResults(msg, false);
                                    }

                                    multipleComponentsCount += 1;
                                }


                                DataTable salesOrderResults = AddUpdateSalesOrder(workOrder, billToResults, buildingResults);
                                DataTable workOrderResults = AddUpdateWorkOrder(workOrder, shipToResults, salesOrderResults, finishItemResults, bomResults, buildingResults);
                                DataTable schedResults = UpdateSchedule(workOrder, workOrderResults, finishItemResults, fileName);
                                
                                // Coproducts
                                Coproducts coproducts = workOrder.WoHdr_Rec.Coproduct;
                                if (coproducts != null)
                                {
                                    string msg = "Coproduct found";
                                    Logger.Log(msg);

                                    ParseResults coproductResults = new ParseResults("", false);
                                    foreach (Coproduct_seq coproduct in coproducts.Coproduct_seq)
                                    {
                                        coproductResults = AddCoproduct(workOrder, coproduct, shipToResults, salesOrderResults, buildingResults, fileName);
                                        if (coproductResults.Pass == false)
                                        {
                                            return coproductResults;
                                        }
                                    }

                                    ParseResults coproductCombinedResults = new ParseResults("", false);
                                    coproductCombinedResults = CombineCoproducts(workOrder);
                                    if (coproductCombinedResults.Pass == false)
                                    {
                                        return coproductCombinedResults;
                                    }
                                }

                                return new ParseResults(validateResults, true); //PASS
                            }
                            else
                                return new ParseResults(validateResults, false);
                        }
                    default:
                        {
                            return new ParseResults("Invalid Status", false);
                        }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                return new ParseResults(ex.Message, false);
            }
        }

        private static ParseResults AddCoproduct(WorkOrder_Info workOrder, Coproduct_seq coproduct, DataTable shipToResults, DataTable salesOrderResults, DataTable buildingResults, string fileName)
        {
            try
            {
                string msg = "";
                DataTable coproductItemResults = AddUpdateCoproductItem(coproduct);

                // Bill of Materials -- Coproducts
                DataTable bomResults = AddUpdateBillOfMaterials(workOrder.WoHdr_Rec.Finish_item, coproductItemResults, GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq));
                if (bomResults.Rows.Count == 0) //Validate
                {
                    msg = "Failed to Get/Add Bill of Material Template - Coproduct";
                    Logger.Log(msg);
                    return new ParseResults(msg, false);
                }

                DataTable stepResults = AddUpdateBOMSteps(workOrder.WoHdr_Rec.Routing, coproduct, bomResults, GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq));
                if (stepResults.Rows.Count == 0) //Validate
                {
                    msg = "Failed to Get/Add Bill of Material Step - Coproduct";
                    Logger.Log(msg);
                    return new ParseResults(msg, false);
                }

                DataTable producedItemCoproductResults = AddUpdateBOMProducedItem(workOrder.WoHdr_Rec.Routing, coproduct, stepResults, coproductItemResults, GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq));
                if (producedItemCoproductResults.Rows.Count == 0) //Validate
                {
                    msg = "Failed to Get/Add Bill of Material Produced Item - Coproduct";
                    Logger.Log(msg);
                    return new ParseResults(msg, false);
                }

                int multipleComponentsCount = 1;
                // Add Required Material for BOM 
                // Components should already be added from the main work order import
                foreach (Component component in workOrder.WoHdr_Rec.Components.Component)
                {
                    DataTable componentItemResults = GetItem(component.comp_item.ToString());
                    if (componentItemResults.Rows.Count == 0) //Validate
                    {
                        msg = "Failed to Get/Add Component Item - Coproduct";
                        Logger.Log(msg);
                        return new ParseResults(msg, false);
                    }

                    DataTable requiredMaterialCoproductResults = AddUpdateBOMRequiredMaterials(component, workOrder.WoHdr_Rec.Routing, stepResults, componentItemResults, GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq), multipleComponentsCount);
                    if (requiredMaterialCoproductResults.Rows.Count == 0) //Validate
                    {
                        msg = "Failed to Get/Add Bill of Material Required Materials - Coproduct";
                        Logger.Log(msg);
                        return new ParseResults(msg, false);
                    }

                    multipleComponentsCount += 1;
                }

                DataTable workOrderResults = AddUpdateWorkOrder(workOrder, shipToResults, salesOrderResults, coproductItemResults, bomResults, buildingResults, coproduct);
                DataTable schedResults = UpdateSchedule(workOrder, workOrderResults, coproductItemResults, fileName, coproduct);

                return new ParseResults(msg, true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                return new ParseResults(ex.Message, false);
            }
        }

        private static ParseResults CombineCoproducts(WorkOrder_Info workOrder)
        {
            try
            {
                string msg = "Trying to combine coproduct Orders";
                Logger.Log(msg);

                //
                DataTable results = new DataTable();
                try
                {
                    string combineMethod = "";
                    combineMethod = workOrder.WoHdr_Rec.Coproduct.Coproduct_seq[0].Coproduct_Layout.ToString().Substring(0,1);


                    List<SqlParameter> parms = new List<SqlParameter>();
                    parms.Add(new SqlParameter("work_order_no", workOrder.WoHdr_Rec.workorder));
                    parms.Add(new SqlParameter("combine_method", combineMethod));

                    results = sql.ExecuteStoredProcedure(CombineCoproductOrders, parms);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex.Message);
                    return new ParseResults(ex.Message, false);
                }

                return new ParseResults(msg, true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                return new ParseResults(ex.Message, false);
            }
        }

        private static Bill_to_Customer GetDefaultBillCustomer()
        {
            return new Bill_to_Customer()
            {
                customerNum = "ReplenishCust",
                Customer_name = "Kelly Spicers",
                Customer_Address_1 = "12202 E. Slauson Ave",
                Customer_Address_2 = "",
                Customer_Address_3 = "",
                Customer_City = "SANTE FE SPRINGS",
                Customer_State = "CA",
                Customer_Zip = "90670",
                Customer_Attention_to = ""

            };
        }

        private static Ship_to_Customer GetDefaultShipCustomer()
        {
            return new Ship_to_Customer()
            {
                ShipTo_Number = "ReplenishCust",
                ShipTo_Name = "Kelly Spicers",
                ShipTo_Address_1 = "12202 E. Slauson Ave",
                ShipTo_Address_2 = "",
                ShipTo_Address_3 = "",
                ShipTo_City = "SANTE FE SPRINGS",
                ShipTo_State = "CA",
                ShipTo_ZipCode = "90670",
                ShipTo_Attention_to = ""

            };
        }

        private static string ValidateWorkOrderData(WorkOrder_Info workOrder)
        {
            string results = "";
            try
            {

                //Building
                results += ValidateBuilding(workOrder.WoHdr_Rec.fg_warehouse);

                //Customer
                if (workOrder.WoHdr_Rec.wo_type.ToUpper() != "REPLENISHMENT")
                {
                    results += ValidateBillToCustomer(workOrder.WoHdr_Rec.Bill_to_Customer);
                    results += ValidateShipToCustomer(workOrder.WoHdr_Rec.Ship_to_Customer);
                }

                foreach (Component component in workOrder.WoHdr_Rec.Components.Component)
                {
                    //Item
                    results = ValidateComponentItem(component);
                }
            }
            catch (Exception ex)
            {
                results += "Missing Components field;";
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static string ValidateComponentItem(Component component)
        {
            string results = "";

            if (component.Cmitem_widthNumeric != null)
            {
                try
                {
                    decimal widthTest = Convert.ToDecimal(component.Cmitem_widthNumeric);
                }
                catch
                {
                    results += "Invalid Component Width; ";
                }
            }
            else
                results += "Invalid Component Width; ";

            return results;
        }

        public static string ValidateBuilding(fg_warehouse building)
        {
            string results = "";

            if (building != null)
            {
                if (building.fgwarehouse == "")
                {
                    results += "Invalid Building; ";
                }
            }
            else
                results += "Invalid Building; ";

            return results;
        }

        public static string ValidateBillToCustomer(Bill_to_Customer customer)
        {
            string results = "";

            //Need a customer number
            if (customer.customerNum == "")
            {
                results += "Invalid BillTo customerNum; ";
            }

            //Need a customer Name
            if (customer.Customer_name == "")
            {
                results += "Invalid BillTo Customer_name; ";

            }

            //Need at least 1 value in address lines
            if (customer.Customer_Address_1 == "" && customer.Customer_Address_2 == "" && customer.Customer_Address_3 == "")
            {
                results += "Invalid BillTo Address; ";
            }

            return results;
        }

        public static string ValidateShipToCustomer(Ship_to_Customer customer)
        {
            string results = "";

            //Need a customer number
            if (customer.ShipTo_Number == "")
            {
                results += "Invalid ShipTo_Number; ";
            }

            //Need a customer Name
            if (customer.ShipTo_Name == "")
            {
                results += "Invalid ShipTo_Name; ";

            }

            //Need at least 1 value in address lines
            if (customer.ShipTo_Address_1 == "" && customer.ShipTo_Address_2 == "" && customer.ShipTo_Address_3 == "")
            {
                results += "Invalid ShipTo Address; ";
            }

            return results;
        }

        public static DataTable AddUpdateSalesOrder(WorkOrder_Info workOrder, DataTable billToCustomer, DataTable buildingResults)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder.WoHdr_Rec.workorder));
                parms.Add(new SqlParameter("customer_id", billToCustomer.Rows[0]["customer_id"]));
                parms.Add(new SqlParameter("customer_address_id", billToCustomer.Rows[0]["address_id"]));
                parms.Add(new SqlParameter("customer_PO", workOrder.WoHdr_Rec.po_Customer_no));
                parms.Add(new SqlParameter("ref_no", workOrder.WoHdr_Rec.customer_order));
                parms.Add(new SqlParameter("building_id", buildingResults.Rows[0]["building_id"]));
                parms.Add(new SqlParameter("order_date", workOrder.WoHdr_Rec.EntryDate));
                parms.Add(new SqlParameter("blanket_po", ""));              //Empty
                parms.Add(new SqlParameter("sales_rep", "0"));              //None

                results = sql.ExecuteStoredProcedure(ImportSalesOrder, parms);

                results = GetSalesOrder(workOrder.WoHdr_Rec.workorder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }
 
            return results;
        }

        public static DataTable UpdateSalesOrderBillToCustomer(WorkOrder_Info workOrder, DataTable billToCustomer, DataTable salesOrderResults)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder.WoHdr_Rec.workorder));
                parms.Add(new SqlParameter("customer_id", billToCustomer.Rows[0]["customer_id"]));
                parms.Add(new SqlParameter("customer_address_id", billToCustomer.Rows[0]["address_id"]));
                parms.Add(new SqlParameter("customer_PO", workOrder.WoHdr_Rec.po_Customer_no));
                parms.Add(new SqlParameter("ref_no", workOrder.WoHdr_Rec.customer_order));
                parms.Add(new SqlParameter("building_id", salesOrderResults.Rows[0]["building_id"]));
                parms.Add(new SqlParameter("blanket_po", ""));              //Empty
                parms.Add(new SqlParameter("sales_rep", "0"));              //None

                results = sql.ExecuteStoredProcedure(ImportSalesOrder, parms);

                results = GetSalesOrder(workOrder.WoHdr_Rec.workorder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateWorkOrder(WorkOrder_Info workOrder, DataTable shipToCustomer, DataTable salesOrder, DataTable itemResults, DataTable bomResults, DataTable buildingResults)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder.WoHdr_Rec.workorder));
                parms.Add(new SqlParameter("sales_order_id", salesOrder.Rows[0]["sales_order_id"]));
                parms.Add(new SqlParameter("ship_customer_id", shipToCustomer.Rows[0]["customer_id"]));
                parms.Add(new SqlParameter("ship_customer_address_id", shipToCustomer.Rows[0]["address_id"]));
                parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("order_qty", workOrder.WoHdr_Rec.Finish_item.cust_order_qty));
                parms.Add(new SqlParameter("order_qty_uom", workOrder.WoHdr_Rec.Finish_item.unit_of_measure));
                parms.Add(new SqlParameter("bom_id", bomResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("template_id", bomResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("mfg_building_id", buildingResults.Rows[0]["building_id"]));
                parms.Add(new SqlParameter("width", System.Convert.ToDecimal(workOrder.WoHdr_Rec.Finish_item.Finish_widthNumeric)));
                parms.Add(new SqlParameter("length", System.Convert.ToDecimal(workOrder.WoHdr_Rec.Finish_item.Finish_lengthNumeric)));
                parms.Add(new SqlParameter("due_date", workOrder.WoHdr_Rec.DueDate));
                parms.Add(new SqlParameter("work_order_no", GetMasterWorkOrderNumber(workOrder)));


                if (workOrder.WoHdr_Rec.wo_type == "REPLENISHMENT")
                {
                    parms.Add(new SqlParameter("order_type_id", WorkOrderTypes.StockReplenishmentOrder));
                }
                else
                {
                    parms.Add(new SqlParameter("order_type_id", WorkOrderTypes.MakeOrder));
                }

                results = sql.ExecuteStoredProcedure(ImportWorkOrder, parms);

                results = GetWorkOrder(workOrder.WoHdr_Rec.workorder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateWorkOrder(WorkOrder_Info workOrder, DataTable shipToCustomer, DataTable salesOrder, DataTable itemResults, DataTable bomResults, DataTable buildingResults, Coproduct_seq coproduct)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq)));
                parms.Add(new SqlParameter("sales_order_id", salesOrder.Rows[0]["sales_order_id"]));
                parms.Add(new SqlParameter("ship_customer_id", shipToCustomer.Rows[0]["customer_id"]));
                parms.Add(new SqlParameter("ship_customer_address_id", shipToCustomer.Rows[0]["address_id"]));
                parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("order_qty", coproduct.Coproduct_order_qty));
                parms.Add(new SqlParameter("order_qty_uom", coproduct.Coproduct_UOM));
                parms.Add(new SqlParameter("bom_id", bomResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("template_id", bomResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("mfg_building_id", buildingResults.Rows[0]["building_id"]));
                parms.Add(new SqlParameter("width", System.Convert.ToDecimal(coproduct.Coproduct_widthNumeric)));
                parms.Add(new SqlParameter("length", System.Convert.ToDecimal(coproduct.Coproduct_lengthNumeric)));
                parms.Add(new SqlParameter("due_date", workOrder.WoHdr_Rec.DueDate));
                parms.Add(new SqlParameter("work_order_no", GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq)));


                if (workOrder.WoHdr_Rec.wo_type == "REPLENISHMENT")
                {
                    parms.Add(new SqlParameter("order_type_id", WorkOrderTypes.StockReplenishmentOrder));
                }
                else
                {
                    parms.Add(new SqlParameter("order_type_id", WorkOrderTypes.MakeOrder));
                }

                results = sql.ExecuteStoredProcedure(ImportWorkOrder, parms);


                //results = GetWorkOrder(workOrder.WoHdr_Rec.workorder + "-" + coproduct.CoproductR_seq);
                results = GetWorkOrder(GetWorkOrderCoProductNumber(workOrder.WoHdr_Rec.workorder, coproduct.CoproductR_seq));
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable UpdateSalesOrderShipToCustomer(WorkOrder_Info workOrder, DataTable shipToCustomer, DataTable salesOrder, DataTable workOrderResults)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder.WoHdr_Rec.workorder));
                parms.Add(new SqlParameter("sales_order_id", salesOrder.Rows[0]["sales_order_id"]));
                parms.Add(new SqlParameter("ship_customer_id", shipToCustomer.Rows[0]["customer_id"]));
                parms.Add(new SqlParameter("ship_customer_address_id", shipToCustomer.Rows[0]["address_id"]));
                parms.Add(new SqlParameter("item_master_id", workOrderResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("order_qty", workOrderResults.Rows[0]["order_qty"]));
                parms.Add(new SqlParameter("order_qty_uom", workOrderResults.Rows[0]["order_qty_uom"]));
                parms.Add(new SqlParameter("bom_id", workOrderResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("template_id", workOrderResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("mfg_building_id", workOrderResults.Rows[0]["building_id"]));
                parms.Add(new SqlParameter("width", workOrderResults.Rows[0]["width"]));
                parms.Add(new SqlParameter("length", workOrderResults.Rows[0]["length"]));


                if (workOrder.WoHdr_Rec.wo_type == "REPLENISHMENT")
                {
                    parms.Add(new SqlParameter("order_type_id", WorkOrderTypes.StockReplenishmentOrder));
                }
                else
                {
                    parms.Add(new SqlParameter("order_type_id", WorkOrderTypes.MakeOrder));
                }

                results = sql.ExecuteStoredProcedure(ImportWorkOrder, parms);

                results = GetWorkOrder(workOrder.WoHdr_Rec.workorder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable UpdateSchedule(WorkOrder_Info workOrder, DataTable workOrderResults, DataTable itemResults, string fileName)
        {
            DataTable results = new DataTable();
            try
            {
                DataTable customFieldResults = UpdateWorkOrderCustomFields(workOrder.WoHdr_Rec, fileName);

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("wo_master_id", workOrderResults.Rows[0]["wo_master_id"]));
                parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));
                
                results = sql.ExecuteStoredProcedure(ScheduleOrder, parms);

            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable UpdateSchedule(WorkOrder_Info workOrder, DataTable workOrderResults, DataTable itemResults, string fileName, Coproduct_seq coproduct)
        {
            DataTable results = new DataTable();
            try
            {
                DataTable customFieldResults = UpdateWorkOrderCustomFields(workOrder.WoHdr_Rec, fileName, coproduct);

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("wo_master_id", workOrderResults.Rows[0]["wo_master_id"]));
                parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));

                results = sql.ExecuteStoredProcedure(ScheduleOrder, parms);

            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable DeleteWorkOrder(WorkOrder_Info workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                DataTable workOrderResults = GetWorkOrder(workOrder.WoHdr_Rec.workorder);
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("SalesOrderID", workOrderResults.Rows[0]["sales_order_id"]));
                parms.Add(new SqlParameter("user_id", -4));

                results = sql.ExecuteStoredProcedure(DeleteOrder, parms);
                
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBillToCustomer(Bill_to_Customer customer, Ship_to_Customer shipCustomer)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", customer.customerNum));
                parms.Add(new SqlParameter("customer_name", customer.Customer_name));
                parms.Add(new SqlParameter("customer_code", customer.customerNum));
                parms.Add(new SqlParameter("is_customer", true));
                if (shipCustomer.ShipTo_Number == customer.customerNum)
                {
                    parms.Add(new SqlParameter("is_vendor", true));
                }
                else
                {
                    parms.Add(new SqlParameter("is_vendor", false));
                }
                parms.Add(new SqlParameter("bill_addr_1", customer.Customer_Address_2));
                parms.Add(new SqlParameter("bill_addr_2", customer.Customer_Address_3));
                parms.Add(new SqlParameter("bill_addr_3", customer.Customer_Address_1));
                parms.Add(new SqlParameter("bill_city", customer.Customer_City));
                parms.Add(new SqlParameter("bill_state", customer.Customer_State));
                parms.Add(new SqlParameter("bill_zip_code", customer.Customer_Zip));
                parms.Add(new SqlParameter("bill_country", "United States"));


                results = sql.ExecuteStoredProcedure(ImportCustomer, parms);

                results = GetCustomer(customer.customerNum, "Bill To", customer.Customer_Address_2, customer.Customer_Address_3, customer.Customer_Address_1,
                    customer.Customer_City, customer.Customer_State, customer.Customer_Zip);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateShipToCustomer(Ship_to_Customer customer, Bill_to_Customer billCustomer)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", customer.ShipTo_Number));
                parms.Add(new SqlParameter("customer_name", customer.ShipTo_Name));
                parms.Add(new SqlParameter("customer_code", customer.ShipTo_Number));
                if (customer.ShipTo_Number == billCustomer.customerNum)
                {
                    parms.Add(new SqlParameter("is_customer", true));
                }
                else
                {
                    parms.Add(new SqlParameter("is_customer", false));
                }
                parms.Add(new SqlParameter("is_vendor", true));
                parms.Add(new SqlParameter("ship_addr_1", customer.ShipTo_Address_2));
                parms.Add(new SqlParameter("ship_addr_2", customer.ShipTo_Address_3));
                parms.Add(new SqlParameter("ship_addr_3", customer.ShipTo_Address_1));
                parms.Add(new SqlParameter("ship_city", customer.ShipTo_City));
                parms.Add(new SqlParameter("ship_state", customer.ShipTo_State));
                parms.Add(new SqlParameter("ship_zip_code", customer.ShipTo_ZipCode));
                parms.Add(new SqlParameter("ship_country", "United States"));

                results = sql.ExecuteStoredProcedure(ImportCustomer, parms);

                results = GetCustomer(customer.ShipTo_Number, "Ship To", customer.ShipTo_Address_2, customer.ShipTo_Address_3, customer.ShipTo_Address_1,
                    customer.ShipTo_City, customer.ShipTo_State, customer.ShipTo_ZipCode);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBuilding(fg_warehouse warehouse)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", warehouse.fgwarehouse));
                parms.Add(new SqlParameter("building_name", warehouse.fgwarehouse));
                parms.Add(new SqlParameter("building_desc", warehouse.FgWarehouse_description));
                parms.Add(new SqlParameter("addr_1", warehouse.FgWarehouse_Address1));
                parms.Add(new SqlParameter("addr_2", warehouse.FgWarehouse_Address2));
                parms.Add(new SqlParameter("addr_3", ""));
                parms.Add(new SqlParameter("city", warehouse.FgWarehouse_City));
                parms.Add(new SqlParameter("state", warehouse.FgWarehouse_State));
                parms.Add(new SqlParameter("zip_code", warehouse.FgWarehouse_ZipCode));
                parms.Add(new SqlParameter("country", "United States"));

                results = sql.ExecuteStoredProcedure(ImportBuilding, parms);

                results = GetBuilding(warehouse.fgwarehouse);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateComponentBuilding(Component component)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", component.component_warehouse));
                parms.Add(new SqlParameter("building_name", component.component_warehouse));
                parms.Add(new SqlParameter("building_desc", component.CmWarehouse_description));
                parms.Add(new SqlParameter("addr_1", component.CmWarehouse_Address1));
                parms.Add(new SqlParameter("addr_2", component.CmWarehouse_Address2));
                parms.Add(new SqlParameter("addr_3", ""));
                parms.Add(new SqlParameter("city", component.CmWarehouse_City));
                parms.Add(new SqlParameter("state", component.CmWarehouse_State));
                parms.Add(new SqlParameter("zip_code", component.CmWarehouse_ZipCode));
                parms.Add(new SqlParameter("country", "United States"));

                results = sql.ExecuteStoredProcedure(ImportBuilding, parms);

                results = GetBuilding(component.component_warehouse);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateFinishItem(Finish_item finish_item)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", finish_item.parent_item.ToString()));
                parms.Add(new SqlParameter("item_number", finish_item.parent_item.ToString()));
                parms.Add(new SqlParameter("item_description", finish_item.parent_description.ToString()));
                parms.Add(new SqlParameter("stock_uom_1", finish_item.unit_of_measure.ToString()));
                parms.Add(new SqlParameter("stock_uom_2", "EA"));

                results = sql.ExecuteStoredProcedure(ImportItem, parms);

                DataTable customFieldResults = AddItemCustomFields(finish_item);

                if (customFieldResults.Rows.Count == 0)
                {
                    Logger.Log("Failed to Get/Add Finish Item - Coproduct");
                    return new DataTable();
                }

                results = GetItem(finish_item.parent_item.ToString());
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateCoproductItem(Coproduct_seq coproduct)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", coproduct.Coproduct_item.ToString()));
                parms.Add(new SqlParameter("item_number", coproduct.Coproduct_item.ToString()));
                parms.Add(new SqlParameter("item_description", coproduct.Coproduct_description.ToString()));
                parms.Add(new SqlParameter("stock_uom_1", coproduct.Coproduct_UOM.ToString()));
                parms.Add(new SqlParameter("stock_uom_2", "EA"));

                results = sql.ExecuteStoredProcedure(ImportItem, parms);

                DataTable customFieldResults = AddItemCustomFields(coproduct);
                
                if (customFieldResults.Rows.Count == 0)
                { 
                    Logger.Log("Failed to Get/Add Finish Item - Coproduct");
                    return new DataTable();
                }

                results = GetItem(coproduct.Coproduct_item.ToString());
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateComponentItem(Component component)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", component.comp_item.ToString()));
                parms.Add(new SqlParameter("item_number", component.comp_item.ToString()));
                parms.Add(new SqlParameter("item_description", component.item_desc));

                results = sql.ExecuteStoredProcedure(ImportItem, parms);

                results = AddItemCustomFields(component);

                results = GetItem(component.comp_item.ToString());
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBillOfMaterials(Finish_item finishItem, DataTable itemResults, string workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder));
                parms.Add(new SqlParameter("bom_name", workOrder));
                //parms.Add(new SqlParameter("ext_id", itemResults.Rows[0]["item_number"]));
                //parms.Add(new SqlParameter("bom_name", itemResults.Rows[0]["item_number"]));
                parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("uom", itemResults.Rows[0]["stock_uom_1"]));
                parms.Add(new SqlParameter("quantity", 1));

                results = sql.ExecuteStoredProcedure(ImportBOM, parms);

                results = GetBOM(workOrder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        private static string GetWorkOrderCoProductNumber(string workOrder, string coproductSequence)
        {
            return workOrder.Replace(workOrder.Substring(0, workOrder.IndexOf("-")), workOrder.Substring(0, workOrder.IndexOf("-")) + coproductSequence);
        }

        private static string GetMasterWorkOrderNumber(WorkOrder_Info workOrder)
        {
            string workOrder_name = workOrder.WoHdr_Rec.workorder;
            if (workOrder.WoHdr_Rec.Coproduct != null)
            {
                return workOrder_name.Replace(workOrder_name.Substring(0, workOrder_name.IndexOf("-")), workOrder_name.Substring(0, workOrder_name.IndexOf("-")) + "A");
            }
            else
            {
                return workOrder_name;
            }
        }

        public static DataTable AddUpdateBillOfMaterials(Coproduct_seq coproduct, DataTable itemResults, string workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq)));
                parms.Add(new SqlParameter("bom_name", GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq)));
                //parms.Add(new SqlParameter("ext_id", itemResults.Rows[0]["item_number"]));
                //parms.Add(new SqlParameter("bom_name", itemResults.Rows[0]["item_number"]));
                parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("uom", itemResults.Rows[0]["stock_uom_1"]));
                parms.Add(new SqlParameter("quantity", 1));

                results = sql.ExecuteStoredProcedure(ImportBOM, parms);

                results = GetBOM(GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq));
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBOMSteps(Routing routing, Finish_item finishItem, DataTable bomResults, string workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                DataTable workCenterGroup = GetWorkCenterGroup(routing.operation_desc);

                if (workCenterGroup.Rows.Count == 0)
                {
                    Logger.Log("Failed to Find Work Center Group");
                    return results;
                }

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder));
                //parms.Add(new SqlParameter("ext_id", finishItem.parent_item.ToString() + routing.routing_seq.ToString()));
                parms.Add(new SqlParameter("template_id", bomResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("step_option_id", 1));
                parms.Add(new SqlParameter("work_center_group_id", workCenterGroup.Rows[0]["work_center_group_id"]));


                results = sql.ExecuteStoredProcedure(ImportBOMStep, parms);

                results = GetBOMStep(workOrder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBOMSteps(Routing routing, Coproduct_seq coproduct, DataTable bomResults, string workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                DataTable workCenterGroup = GetWorkCenterGroup(routing.operation_desc);

                if (workCenterGroup.Rows.Count == 0)
                {
                    Logger.Log("Failed to Find Work Center Group - Coproduct");
                    return results;
                }

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq)));
                //parms.Add(new SqlParameter("ext_id", finishItem.parent_item.ToString() + routing.routing_seq.ToString()));
                parms.Add(new SqlParameter("template_id", bomResults.Rows[0]["template_id"]));
                parms.Add(new SqlParameter("step_option_id", 1));
                parms.Add(new SqlParameter("work_center_group_id", workCenterGroup.Rows[0]["work_center_group_id"]));


                results = sql.ExecuteStoredProcedure(ImportBOMStep, parms);

                results = GetBOMStep(GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq));
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBOMProducedItem(Routing routing, Finish_item finishItem, DataTable stepResults, DataTable finishItemResults, string workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder));
                parms.Add(new SqlParameter("step_id", stepResults.Rows[0]["step_id"]));
                parms.Add(new SqlParameter("item_master_id", finishItemResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("quantity", 1));
                parms.Add(new SqlParameter("uom", finishItemResults.Rows[0]["stock_uom_1"]));
                parms.Add(new SqlParameter("transaction_option_id", 2));                    //OFF-FIN

                results = sql.ExecuteStoredProcedure(ImportBOMProducedItem, parms);

                results = GetBOMProducedItem(workOrder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBOMProducedItem(Routing routing, Coproduct_seq coproduct, DataTable stepResults, DataTable finishItemResults, string workOrder)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq)));
                parms.Add(new SqlParameter("step_id", stepResults.Rows[0]["step_id"]));
                parms.Add(new SqlParameter("item_master_id", finishItemResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("quantity", 1));
                parms.Add(new SqlParameter("uom", finishItemResults.Rows[0]["stock_uom_1"]));
                parms.Add(new SqlParameter("transaction_option_id", 2));                    //OFF-FIN

                results = sql.ExecuteStoredProcedure(ImportBOMProducedItem, parms);

                results = GetBOMProducedItem(GetWorkOrderCoProductNumber(workOrder, coproduct.CoproductR_seq));
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateBOMRequiredMaterials(Component component, Routing routing, DataTable stepResults, DataTable componentResults, string workOrder, int multipleComponentCount)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", workOrder + "-" + multipleComponentCount.ToString()));
                parms.Add(new SqlParameter("step_id", stepResults.Rows[0]["step_id"]));
                parms.Add(new SqlParameter("item_master_id", componentResults.Rows[0]["item_master_id"]));
                parms.Add(new SqlParameter("quantity", 1)); //Convert.ToDecimal(component.qty_order) / Convert.ToDecimal(finishItem.cust_order_qty)));
                parms.Add(new SqlParameter("uom", "FT"));

                results = sql.ExecuteStoredProcedure(ImportBOMRequiredMaterial, parms);

                results = GetBOMRequiredMaterial(workOrder + "-" + multipleComponentCount.ToString());
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateOnHandInventory(Component component, DataTable itemResults, DataTable buildingResults)
        {
            DataTable results = new DataTable();
            try
            {
                foreach (Location location in component.Inventory.Location) {
                    DataTable locationResults = AddUpdateLocation(location, buildingResults);
                    if (locationResults.Rows.Count == 0)
                    {
                        Logger.Log("Failed to Get/Add Location");
                        return new DataTable();
                    }

                    DataTable itemChildResults = AddItemChildCustomFields(component, itemResults);
                    if (itemChildResults.Rows.Count == 0)
                    {
                        Logger.Log("Failed to Get/Add Item Child");
                        return new DataTable();
                    }


                    List<SqlParameter> parms = new List<SqlParameter>();
                    parms.Add(new SqlParameter("ext_id", location.lot_no));
                    parms.Add(new SqlParameter("inventory_id", location.lot_no ));
                    parms.Add(new SqlParameter("pallet_id", location.lot_no));
                    parms.Add(new SqlParameter("stock_uom_weight", location.Qty_Oh));
                    parms.Add(new SqlParameter("stock_uom_1", location.linealFeet));
                    parms.Add(new SqlParameter("item_master_id", itemResults.Rows[0]["item_master_id"]));
                    parms.Add(new SqlParameter("location_id", locationResults.Rows[0]["location_id"]));
                    parms.Add(new SqlParameter("item_child_id", itemChildResults.Rows[0]["item_child_id"]));
                    parms.Add(new SqlParameter("is_consigned", false));
                    parms.Add(new SqlParameter("unit_cost_uom_id", "LB"));

                    results = sql.ExecuteStoredProcedure(ImportInventory, parms);

                    UpdateInventoryAssignSeq(location);

                    results = GetOnHandInventory(location.lot_no);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddUpdateLocation(Location location, DataTable buildingResults)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("ext_id", location.location + "-" + buildingResults.Rows[0]["building_name"]));
                parms.Add(new SqlParameter("building_id", buildingResults.Rows[0]["building_id"]));
                parms.Add(new SqlParameter("location_name", location.location + "-" + buildingResults.Rows[0]["building_name"]));
                parms.Add(new SqlParameter("location_desc", location.location));
                

                results = sql.ExecuteStoredProcedure(ImportLocation, parms);

                results = GetLocation(location.location + "-" + buildingResults.Rows[0]["building_name"]) ;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetWorkOrderDataForExport(long id)
        {
            DataTable results = new DataTable();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("work_order_id", id));

                results = sql.ExecuteStoredProcedure(ExportWorkOrder, parms);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetCustomer(string customer_num, string customer_type, string address1, string address2, string address3, string city, string state, string zip)
        {
            DataTable results = new DataTable();
            try
            {
                string addType = customer_type == "Bill To" ? "1" : "0";

                string qry = "SELECT TOP 1 cs.customer_id, cs.ext_id, cs.customer_name, cs.customer_code, ca.address_id FROM cs_customers cs " +
                    "JOIN cs_customer_addresses caa on cs.customer_id = caa.customer_id " +
                    "JOIN cs_addresses ca on caa.address_id = ca.address_id " +
                    "where cs.ext_id = '" + customer_num + "' " +
                    "AND caa.address_type_id = " + addType + " " +
                    "AND ca.address = '" + address1 + "' " +
                    "AND ca.address_2 = '" + address2 + "' " +
                    "AND ca.address_3 = '" + address3 + "' " +
                    "AND ca.city = '" + city + "' " +
                    "AND ca.state = '" + state + "' " +
                    "AND ca.zip_code = '" + zip + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetSalesOrder(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 sales_order_id, building_id from so_sales_orders where ext_id = '" + ext_id + "'";


                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetWorkOrder(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 wm.wo_master_id, wm.work_order_no, so.sales_order_id, so.sales_order_no, wm.order_qty, wm.order_qty_uom, wm.bom_id, wm.template_id, wm.mfg_building_id, " +
                    "ic.width, ic.length, im.item_master_id " +
                    "from wo_master wm " +
                    "JOIN so_children soc on wm.work_order_no = soc.work_order_no " +
                    "JOIN so_sales_orders so on soc.sales_order_id = so.sales_order_id " +
                    "JOIN ic_item_children ic on wm.item_child_id = ic.item_child_id " +
                    "JOIN ic_item_master im on ic.item_master_id = im.item_master_id " +
                    "where wm.ext_id = '" + ext_id + "'";


                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetItem(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 im.item_master_id, im.item_number, im.item_description, ic.item_child_id, im.stock_uom_1 from ic_item_master im " +
                    "LEFT JOIN ic_item_children ic on im.item_master_id = ic.item_master_id where im.ext_id = '" + ext_id + "'";


                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetBOM(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 template_id, ext_id from bom_templates where ext_id = '" + ext_id + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetBOMStep(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 step_id, ext_id from bom_steps where ext_id = '" + ext_id + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetBOMProducedItem(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 produced_item_id, ext_id from bom_produced_items where ext_id = '" + ext_id + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetBOMRequiredMaterial(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 required_material_id, ext_id from bom_required_materials where ext_id = '" + ext_id + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetLocation(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 location_id from cs_locations where ext_id = '" + ext_id + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetBuilding(string ext_id)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 building_id, building_name from cs_buildings where ext_id = '" + ext_id + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetWorkCenterGroup(string groupName)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 work_center_group_id from sf_work_center_groups where work_center_group_name = '" + groupName + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable GetOnHandInventory(string serial_number)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "SELECT TOP 1 on_hand_inventory_id, inventory_id from ic_on_hand_inventory where inventory_id = '" + serial_number + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable UpdateWorkOrderCustomFields(WoHdr_Rec workOrder, string fileName)
        {
            DataTable results = new DataTable();
            try
            {
                string qry = "UPDATE wo_master SET " +
                    "last_modified_date = '" + DateTime.Now.ToString() + "', " +
                    "last_modified_by = -4," +
                    "units_per_skid = " + workOrder.Finish_item.Finish_PAL_Qty + ", " + 
                    "units = '" + workOrder.Finish_item.Finish_PAL_UOM + "', " + 
                    "company_id = '" + workOrder.company.ToString() + "', " +
                    "ext_workorder = '" + workOrder.workorder.ToString() + "', " +
                    "customer_job_number = '" + workOrder.Job_Number.ToString() + "', " +
                    "file_name = '" + fileName + "' " +
                    "WHERE ext_id = '" + workOrder.workorder.ToString() + "'; " +
                    "SELECT TOP 1 wo_master_id from wo_master WHERE ext_id = '" + workOrder.workorder.ToString() + "'; ";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable UpdateWorkOrderCustomFields(WoHdr_Rec workOrder, string fileName, Coproduct_seq coproduct)
        {
            DataTable results = new DataTable();
            try
            {
                string qry = "UPDATE wo_master SET " +
                    "last_modified_date = '" + DateTime.Now.ToString() + "', " +
                    "last_modified_by = -4," +
                    "units_per_skid = " + coproduct.Coproduct_PAL_Qty + ", " +
                    "units = '" + coproduct.Coproduct_PAL_UOM + "', " +
                    "company_id = '" + workOrder.company.ToString() + "', " +
                    "ext_workorder = '" + GetWorkOrderCoProductNumber(workOrder.workorder.ToString(), coproduct.CoproductR_seq) + "', " +
                    "customer_job_number = '" + workOrder.Job_Number.ToString() + "', " +
                    "file_name = '" + fileName + "' " +
                    "WHERE ext_id = '" + GetWorkOrderCoProductNumber(workOrder.workorder.ToString(), coproduct.CoproductR_seq) + "'; " +
                    "SELECT TOP 1 wo_master_id from wo_master WHERE ext_id = '" + GetWorkOrderCoProductNumber(workOrder.workorder.ToString(), coproduct.CoproductR_seq) + "'; ";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddItemCustomFields(Finish_item finishItem)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "UPDATE ic_item_master SET " +
                    "last_modified_date = '" + DateTime.Now.ToString() + "', " +
                    "last_modified_by = -4," +
                    "basis_weight = '" + finishItem.FinishBasisCaliper.ToString() + "', " +
                    "grade = '" + finishItem.FinishGrade.ToString() + "', " +
                    "caliper = " + finishItem.Finish_dimensionNumeric.ToString() + ", " +
                    "m_weight = " + finishItem.mweight.ToString() + ", " +
                    "width = " + finishItem.Finish_widthNumeric.ToString() + ", " +
                    "length = " + finishItem.Finish_lengthNumeric.ToString() + ", " +
                    "Finish_Width_Alpha = '" + finishItem.Finish_WidthAlpha.ToString() + "', " +
                    "Finish_Length_Alpha = '" + finishItem.Finish_LengthAlpha.ToString() + "', " +
                    "SFI_Description = '" + finishItem.SFI_Description.ToString() + "' " +
                    "WHERE ext_id = '" + finishItem.parent_item.ToString() + "'; " +
                    "SELECT TOP 1 item_master_id from ic_item_master WHERE ext_id = '" + finishItem.parent_item.ToString() + "'; ";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddItemCustomFields(Coproduct_seq coproduct)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "UPDATE ic_item_master SET " +
                    "last_modified_date = '" + DateTime.Now.ToString() + "', " +
                    "last_modified_by = -4," +
                    "basis_weight = '" + coproduct.Coproduct_BasisCaliper.ToString() + "', " +
                    "grade = '" + coproduct.Coproduct_Grade.ToString() + "', " +
                    "caliper = " + coproduct.Coproduct_dimensionNumeric.ToString() + ", " +
                    "m_weight = " + coproduct.Coproduct_mweight.ToString() + ", " +
                    "width = " + coproduct.Coproduct_widthNumeric.ToString() + ", " +
                    "length = " + coproduct.Coproduct_lengthNumeric.ToString() + ", " +
                    "Finish_Width_Alpha = '" + coproduct.Coproduct_WidthAlpha.ToString() + "', " +
                    "Finish_Length_Alpha = '" + coproduct.Coproduct_LengthAlpha.ToString() + "', " +
                    "SFI_Description = '" + coproduct.Coproduct_SFI_Description.ToString() + "' " +
                    "WHERE ext_id = '" + coproduct.Coproduct_item.ToString() + "'; " +
                    "SELECT TOP 1 item_master_id from ic_item_master WHERE ext_id = '" + coproduct.Coproduct_item.ToString() + "'; ";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddItemCustomFields(Component component)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "UPDATE ic_item_master SET " +
                    "last_modified_date = '" + DateTime.Now.ToString() + "', " +
                    "last_modified_by = -4," +
                    "width = " + Convert.ToDecimal(component.Cmitem_widthNumeric).ToString() + ", " +
                    "roll_od = " + Convert.ToDecimal(component.Comp_RollSize) + " " +
                    "WHERE ext_id = '" + component.comp_item.ToString() + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable UpdateInventoryAssignSeq(Location location)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "UPDATE ic_on_hand_inventory SET " +
                    "last_modified_date = '" + DateTime.Now.ToString() + "', " +
                    "last_modified_by = -4," +
                    "export_assign_seq = '" + location.assign_seq.ToString() + "' " +
                    "WHERE ext_id = '" + location.lot_no.ToString() + "'";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }

        public static DataTable AddItemChildCustomFields(Component component, DataTable itemResults)
        {
            DataTable results = new DataTable();
            try
            {

                string qry = "";

                qry = "EXEC ic_GetItemChild_Dynamic " +
                    itemResults.Rows[0]["item_master_id"].ToString() + ", " +
                    "',width,length', " +
                    "', " + component.Cmitem_widthNumeric.ToString() + ", 0" + "', " +
                    "'AND width = " + component.Cmitem_widthNumeric.ToString() + " AND length = 0" + "', " +
                    "0";

                results = sql.ExcecuteTransactionQuery(qry);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return results;
        }
    }

    public enum WorkOrderTypes
    {
        MakeOrder = 1,
        StockOrder = 2,
        StockReplenishmentOrder = 3,
        DirectSale = 4,
        Rework = 5
    }
}