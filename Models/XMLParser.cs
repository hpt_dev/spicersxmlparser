using System.Xml;
using System.IO;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Data.SqlClient;
using parser.Models.IncomingXML;
using System.Data;
using System.Net;
using parser.Models.Log;
using System.Threading.Tasks;
using WinSCP;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Microsoft.Extensions.Options;

namespace parser.Models
{
    public class XMLParser
    {
        public string ParseDirectory { get; set; } = "";
        public string FailDirectory { get; set; } = "";
        public string PassDirectory { get; set; } = "";
        public string ParseFTPDirectory { get; set; } = "";
        public string FailFTPDirectory { get; set; } = "";
        public string PassFTPDirectory { get; set; } = "";
        public SessionOptions FTPSessionOptions { get; set; }

        public bool Running { get; set; }

        public FileSystemWatcher fileSystemWatcher;

        public bool Init(string directory, string failDirectory, string passDirectory)
        {
            ParseDirectory = directory;
            FailDirectory = failDirectory;
            PassDirectory = passDirectory;
            Running = false;

            TestConnection();
            return true;
        }

        public bool Init(string directory)
        {
            ParseDirectory = directory;
            FailDirectory = ParseDirectory + "\\failed\\";
            PassDirectory = ParseDirectory + "\\passed\\";

            Running = false;
            TestConnection();
            return true;
        }

        public bool Init()
        {
            try
            {         
                GetDirectories();

                TestConnection();
                return true;
            }
            catch (Exception)
            {
                Console.Out.Write("Fail Init Parser");
            }

            return false;
            
        }

        public bool TestConnection()
        {
            fileSystemWatcher = new FileSystemWatcher(ParseDirectory);

            return true;
        }

        public bool StartMonitoringDirectory()
        {
            //SessionOptions options = new SessionOptions()
            //{
            //    Protocol = Protocol.Ftp,
            //    HostName = "64.197.113.13",
            //    UserName = "Redhawk",
            //    Password = "!RD583xtt",
            //    PortNumber = 21
            //};

            Running = true;

            if (TestFTPConnection(FTPSessionOptions)) //Test connection first, if success, start monitoring
            {
                using (Session session = new Session())
                {
                    // Connect
                    session.Open(FTPSessionOptions);
                    Logger.Log("Connected to FTP");

                    List<string> prevFiles = null;

                    while (Running)
                    {
                        try
                        {
                            // Collect file list
                            // Ordered By Last Write Time ASC
                            List<string> files = session.EnumerateRemoteFiles(ParseFTPDirectory, "*.xml", WinSCP.EnumerationOptions.AllDirectories)
                                .ToList<RemoteFileInfo>()
                                .OrderBy(x => x.LastWriteTime)
                                .Select(x => x.Name)
                                .ToList();

                            if (prevFiles == null)
                            {
                                // In the first round, just print number of files found
                                Logger.Log("Found " + files.Count.ToString() + " Existing files");
                                // Then look for differences against the previous list
                                IEnumerable<string> added = files;
                                if (added.Any())
                                {
                                    foreach (string fileName in added)
                                    {
                                        Logger.Log("Processing FTP File: " + fileName);
                                        ProcessFTPFile(session, fileName);
                                    }
                                }
                            }
                            else
                            {
                                // Then look for differences against the previous list
                                IEnumerable<string> added = files.Except(prevFiles);
                                if (added.Any())
                                {
                                    foreach (string fileName in added)
                                    {
                                        Logger.Log("Processing FTP File: " + fileName);
                                        ProcessFTPFile(session, fileName);
                                    }
                                }

                                IEnumerable<string> removed = files.Except(files);
                                if (removed.Any())
                                {
                                    Debug.WriteLine("Removed files:");
                                    foreach (string path in removed)
                                    {
                                        Debug.WriteLine(path);
                                    }
                                }
                            }

                            prevFiles = files;

                            Debug.WriteLine("Sleeping 10s...");
                            Thread.Sleep(10000);
                        }
                        catch (Exception ex)
                        {
                            Logger.Log("FTP Fail: " + ex.Message);
                            Debug.WriteLine(ex.Message);
                            Running = false;
                        }
                    }
                }
            }

            return true;
        }

        private bool TestFTPConnection(SessionOptions options)
        {
            Session testSession = new Session();
            try
            {
                Logger.Log("Testing FTP Connection");
                testSession.Open(options);
                testSession.Close();

                return true;
            }
            catch
            {
                Logger.Log("FTP Failed to Connect");

                Logger.Log("Running in Local Mode");
                ParseExistingFiles();

                fileSystemWatcher.EnableRaisingEvents = true;
                fileSystemWatcher.Created += new FileSystemEventHandler(FileCreated);
                Logger.Log("Starting Parser");

                return false;
            }
        }

        public bool ProcessFTPFile(Session session, string fileName)
        {
            try
            {
                string filePath = ParseDirectory + "\\" + fileName;               

                session.GetFileToDirectory(ParseFTPDirectory + fileName, ParseDirectory);
                ParseResults results = ReadXML(filePath);
                if (results.Pass)
                {
                    Logger.Log("Moving File " + fileName);
                    Logger.Log(ParseFTPDirectory + fileName + " ==> " + PassFTPDirectory + fileName);
                    
                    session.MoveFile(ParseFTPDirectory + fileName, PassFTPDirectory + fileName);
                }
                else
                {
                    Logger.Log("Moving File " + fileName);
                    Logger.Log(ParseFTPDirectory + fileName + " ==> " + FailFTPDirectory + fileName);

                    session.PutFileToDirectory(FailDirectory + "\\" + fileName, FailFTPDirectory);
                    session.RemoveFile(ParseFTPDirectory + fileName);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                return false;
            }
        }

        public bool StopMonitoringDirectory()
        {
            fileSystemWatcher.Created -= new FileSystemEventHandler(FileCreated);
            fileSystemWatcher.EnableRaisingEvents = false;
            Logger.Log("Stopping Parser");

            return true;
        }

        //Event fires when a new file is created in the parsing directory
        private void FileCreated(Object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.ToUpper().EndsWith(".XML"))
            {
                ReadXML(e.FullPath);
            }
        }

        private bool ParseExistingFiles()
        {
            Logger.Log("Parsing Existing Files");
            foreach(string file in Directory.GetFiles(ParseDirectory))
            {
                if (file.ToUpper().EndsWith(".XML"))
                {
                    ReadXML(file);
                }
            }

            Logger.Log("Finished Parsing Existing Files");

            return true;
        }

        

        public ParseResults ReadXML(string filePath)
        {
            string uniqueID = "";
 
            try
            {
                Logger.Log("Found File - Waiting");

                Thread.Sleep(2000);
                Logger.Log("Proceeding");

                ParseResults results = new ParseResults("", true);
                
                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                string xml = File.ReadAllText(filePath);

                XMLDatabase.SaveXMLFileToDatabase(GetFileNameFromPath(filePath), xml, "IN");

                string fileType = doc.FirstChild.Name;
                switch (fileType)
                {
                    case "WorkOrder_Info":
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(WorkOrder_Info));
                            using (StringReader reader = new StringReader(xml))
                            {
                                try
                                {
                                    var test = (WorkOrder_Info)serializer.Deserialize(reader);
                                    uniqueID = test.WoHdr_Rec.workorder;
                                    Logger.Log(test.WoHdr_Rec.workorder.ToString());
                                    results = PHXDatabase.SyncWorkOrder(test, GetFileNameFromPath(filePath));
                                }
                                catch
                                {
                                    //Prevent files from not closing
                                    Logger.Log("Closing Reader");
                                    reader.Close();
                                }
                            }
                            return ProcessParseResults(results, filePath, uniqueID);
                        }
                    default:
                        {
                            Logger.Log("Unrecognized XML File Type");
                            return ProcessParseResults(new ParseResults("Unrecognized XML File  Type", false), filePath, uniqueID);
                        }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                return ProcessParseResults(new ParseResults(ex.Message, false), filePath, uniqueID);
            }
        }

        public ParseResults ProcessParseResults(ParseResults results, string filePath, string uniqueID)
        {
            if (results.Pass == true)
            {
                PassFile(filePath, uniqueID);
            }
            else
            {
                FailFile(results.Message, filePath, uniqueID);
            }

            return results;
        }

        public void FixEmptyNodes(string xml)
        {
            using (XmlReader reader = XmlReader.Create(xml))
            {
                while (reader.Read())
                {
                    if (reader.IsEmptyElement)
                    {
                        Logger.Log(reader.Name);
                    }
                }
            }
        }

        public bool PassFile(string filePath, string uniqueID)
        {
            try
            {
                string fileName = GetFileNameFromPath(filePath);
                Logger.Log(fileName);

                if (File.Exists(PassDirectory + "\\" + fileName))
                {
                    Logger.Log("File Exists - Deleting");
                    File.Delete(PassDirectory + "\\" + fileName);
                }

                File.Move(filePath, PassDirectory + "\\" + fileName);

                WriteFileToDatabase(filePath, fileName, "Pass", "", uniqueID, "", "", "");

                Logger.Log("Passed");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool FailFile(string message, string filePath, string uniqueID)
        {
            try
            {
                string fileName = GetFileNameFromPath(filePath);
                Logger.Log(fileName);
                if (File.Exists(FailDirectory + "\\" + fileName))
                {
                    Logger.Log("File Exists - Deleting");
                    File.Delete(FailDirectory + "\\" + fileName);
                }

                Logger.Log("Moving File");

                //Add reason to file - Which will be used when we upload file to fail directory in FTP
                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlElement failReason = doc.CreateElement("FailReason");
                failReason.InnerText = message;

                doc.FirstChild.PrependChild(failReason);

                doc.Save(FailDirectory + "\\" + fileName);
                File.Delete(filePath);

                //File.Move(filePath, _fail_directory + "\\" + fileName);

                WriteFileToDatabase(filePath, fileName, "Fail", message, uniqueID, "", "", "");

                Logger.Log("Failed");
            }
            catch (Exception ex)
            {
                Logger.Log("Error: " + ex.Message);
            }

            return true;
        }

        public bool ResendFile(string filePath)
        {
            try
            {
                string fileName = GetFileNameFromPath(filePath);

                Logger.Log(fileName);

                if (File.Exists(ParseDirectory + "\\" + fileName) && (filePath != ParseDirectory + fileName))
                {
                    Logger.Log("File Exists - Deleting");
                    File.Delete(ParseDirectory + "\\" + fileName);
                }

                File.Move(filePath, ParseDirectory + "\\" + fileName);
                Logger.Log("Passed");
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool WriteFileToDatabase(string filePath, string fileName, string status, string message, string uniqueID, string extWorkOrder, string workOrder, string id)
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("@fileName", fileName));
                parms.Add(new SqlParameter("@filePath", filePath));
                parms.Add(new SqlParameter("@status", status));
                parms.Add(new SqlParameter("@message", message));
                parms.Add(new SqlParameter("@uniqueID", uniqueID));
                parms.Add(new SqlParameter("@ext_workorder", extWorkOrder));
                parms.Add(new SqlParameter("@work_order", workOrder));
                parms.Add(new SqlParameter("@wo_master_id", id));

                XMLDatabase.sql.ExecuteStoredProcedure("dbo.spAddUpdate_IncomingFile", parms);
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool UpdateDirectory(string parseDirectory, string passDirectory, string failDirectory)
        {
            try
            {
                ParseDirectory = parseDirectory;
                PassDirectory = passDirectory;
                FailDirectory = failDirectory;

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("@parse", parseDirectory));
                parms.Add(new SqlParameter("@pass", passDirectory));
                parms.Add(new SqlParameter("@fail", failDirectory));

                XMLDatabase.sql.ExecuteStoredProcedure("dbo.spUpdate_IncomingDirectories", parms);

                return true;
            }
            catch (Exception)
            {

                
            }

            return false;
        }

        public bool GetDirectories()
        {
            try
            {
                if (ParseDirectory == "")
                {
                    List<SqlParameter> parms = new List<SqlParameter>();


                    DataTable dtResults = XMLDatabase.sql.ExecuteStoredProcedure("dbo.spGet_Directories", parms);
                    ParseDirectory = dtResults.Rows[0]["incoming_drop"].ToString();
                    PassDirectory = dtResults.Rows[0]["incoming_passed"].ToString();
                    FailDirectory = dtResults.Rows[0]["incoming_failed"].ToString();

                    ParseFTPDirectory = dtResults.Rows[0]["ftp_incoming_directory"].ToString();
                    PassFTPDirectory = dtResults.Rows[0]["ftp_incoming_pass"].ToString();
                    FailFTPDirectory = dtResults.Rows[0]["ftp_incoming_fail"].ToString();

                    FTPSessionOptions = new SessionOptions()
                    {
                        Protocol = Protocol.Ftp,
                        HostName = dtResults.Rows[0]["ftp_host"].ToString(),
                        UserName = dtResults.Rows[0]["ftp_username"].ToString(),
                        Password = dtResults.Rows[0]["ftp_password"].ToString(),
                        PortNumber = Convert.ToInt32(dtResults.Rows[0]["ftp_port"])
                    };
                }

                return true;
            }
            catch (Exception)
            {
                Console.Out.WriteLine("GetDirectories Failed");
            }

            return false;
        }

        private string GetFileNameFromPath(string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf("\\") + 1, filePath.Length - filePath.LastIndexOf("\\") - 1);
        }
    }
}