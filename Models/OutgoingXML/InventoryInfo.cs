﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace parser.Models.OutgoingXML
{
    public class InventoryInfo
    {

        [XmlRoot(ElementName = "Inventory")]
        public class Inventory
        {
            [XmlElement(ElementName = "id")]
            public int id { get; set; }

            [XmlElement(ElementName = "created_date")]
            public string created_date { get; set; }

            [XmlElement(ElementName = "last_modified_date")]
            public string last_modified_date { get; set; }

            [XmlElement(ElementName = "is_active")]
            public bool is_active { get; set; }

            [XmlElement(ElementName = "item_id")]
            public int item_id { get; set; }

            [XmlElement(ElementName = "item_number")]
            public string item_number { get; set; }
        }
    }
}
