﻿
namespace parser.Models.OutgoingXML
{
    public enum OutgoingTypes
    {
        Inventory,
        RawInventory,
        WorkOrderUpdate
    }
}
