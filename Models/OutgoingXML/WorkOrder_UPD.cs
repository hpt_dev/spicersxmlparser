﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace parser.Models.OutgoingXML
{
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(WorkOrder_UPD));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (WorkOrder_UPD)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "Finish_item")]
    public class Finish_item
    {

        [XmlElement(ElementName = "Finish_parent_item")]
        public string parent_item { get; set; }

        [XmlElement(ElementName = "Finish_parent_description")]
        public string parent_description { get; set; }

        [XmlElement(ElementName = "Finish_Item_Qty")]
        public string Item_Qty { get; set; }

        [XmlElement(ElementName = "Finish_LB")]
        public string LB { get; set; }

        [XmlElement(ElementName = "Finish_work_center")]
        public string work_center { get; set; }

        [XmlElement(ElementName = "Finish_Run_Labr_hrs")]
        public string Run_Labr_hrs { get; set; }

        [XmlElement(ElementName = "Finish_Run_Labr_units")]
        public string Run_Labr_units { get; set; }

        [XmlElement(ElementName = "Finish_Run_Labr_Rate")]
        public string Run_Labr_Rate { get; set; }

        [XmlElement(ElementName = "Finish_Unit_of_Measure")]
        public string Unit_of_Measure { get; set; }

        [XmlElement(ElementName = "Finish_Inventory")]
        public ProductionInventory Inventory { get; set; }

        [XmlElement(ElementName = "Coproducts")]
        public Coproducts Coproducts { get; set; }
    }

    [XmlRoot(ElementName = "Component")]
    public class Component
    {
        [XmlElement(ElementName = "Component_item")]
        public string item { get; set; }

        [XmlElement(ElementName = "Component_warehouse")]
        public string Component_warehouse { get; set; }

        [XmlElement(ElementName = "Component_item_description")]
        public string item_description { get; set; }

        [XmlElement(ElementName = "Component_Inventory")]
        public RawInventory RawInventory { get; set; }
    }

    [XmlRoot(ElementName = "Finish_Inventory")]
    public class ProductionInventory
    {
        [XmlElement(ElementName = "Finish_Location")]
        public List<ProductionLocation> Location { get; set; }
    }

    [XmlRoot(ElementName = "Coproducts")]
    public class Coproducts
    {
        [XmlElement(ElementName = "CFinish_Inventory")]
        public List<CFinish_Inventory> CFinish_Inventory { get; set; }
    }

    [XmlRoot(ElementName = "CFinish_Inventory")]

    public class CFinish_Inventory
    {
        [XmlElement(ElementName = "Coproduct_seq")]
        public string Coproduct_seq { get; set; }

        [XmlElement(ElementName = "Coproduct_item")]
        public string Coproduct_item { get; set; }

        [XmlElement(ElementName = "CFinish_Location")]
        public List<CFinish_Location> CFinish_Location { get; set; }

        
    }

    [XmlRoot(ElementName = "CFinish_Location")]
    public class CFinish_Location
    {
        [XmlElement(ElementName = "CFinish_assign_seq")]
        public string CFinish_assign_seq { get; set; }

        [XmlElement(ElementName = "CFinish_lot_no")]
        public string CFinish_lot_no { get; set; }
        

        [XmlElement(ElementName = "CFinish_Qty_Oh")]
        public string CFinish_Qty_Oh { get; set; }

        [XmlElement(ElementName = "CFinish_Qty_UOM")]
        public string CFinish_Qty_UOM { get; set; }

        [XmlElement(ElementName = "CFinish_Qty_Oh_2")]
        public string CFinish_Qty_Oh_2 { get; set; }

        [XmlElement(ElementName = "CFinish_Qty_UOM_2")]
        public string CFinish_Qty_UOM_2 { get; set; }

        [XmlElement(ElementName = "CFinish_mweight")]
        public string CFinish_mweight { get; set; }
    }

    [XmlRoot(ElementName = "Component_Inventory")]
    public class RawInventory
    {
        [XmlElement(ElementName = "Component_Location")]
        public List<RawLocation> Location { get; set; }
    }

    [XmlRoot(ElementName = "Finish_Location")]
    public class ProductionLocation
    {
        [XmlElement(ElementName = "Finish_assign_seq")]
        public string assign_seq { get; set; }

        //[XmlElement(ElementName = "Finish_location")]
        //public string location { get; set; }

        [XmlElement(ElementName = "Finish_lot_no")]
        public string lot_no { get; set; }

        [XmlElement(ElementName = "Finish_Qty_Oh")]
        public string Qty_Oh { get; set; }

        [XmlElement(ElementName = "Finish_Qty_UOM")]
        public string Qty_UOM { get; set; }

        [XmlElement(ElementName = "Finish_Qty_Oh_2")]
        public string Qty_Oh_2 { get; set; }

        [XmlElement(ElementName = "Finish_Qty_UOM_2")]
        public string Qty_UOM_2 { get; set; }

        [XmlElement(ElementName = "Finish_uom")]
        public string uom { get; set; }

        [XmlElement(ElementName = "Finish_mweight")]
        public string mweight { get; set; }
    }

    [XmlRoot(ElementName = "Component_Location")]
    public class RawLocation
    {
        [XmlElement(ElementName = "Component_assign_seq")]
        public string assign_seq { get; set; }

        [XmlElement(ElementName = "Component_location")]
        public string location { get; set; }

        [XmlElement(ElementName = "Component_lot_no")]
        public string lot_no { get; set; }

        [XmlElement(ElementName = "Component_Used")]
        public string Qty_Oh { get; set; }

        [XmlElement(ElementName = "Component_uom")]
        public string uom { get; set; }

        //[XmlElement(ElementName = "Component_mweight")]
        //public string mweight { get; set; }
    }

    [XmlRoot(ElementName = "Scrap")]
    public class Scrap
    {

        [XmlElement(ElementName = "Scrap_Item")]
        public string Scrap_Item { get; set; }

        [XmlElement(ElementName = "Scrap_Qty")]
        public string Scrap_Qty { get; set; }

        [XmlElement(ElementName = "UOM")]
        public string UOM { get; set; }
    }

    [XmlRoot(ElementName = "WoHdr_Rec")]
    public class WoHdr_Rec
    {

        [XmlElement(ElementName = "wo_status")]
        public string wo_status { get; set; }

        [XmlElement(ElementName = "UniqueID")]
        public string UniqueID { get; set; }

        [XmlElement(ElementName = "company")]
        public string company { get; set; }

        [XmlElement(ElementName = "workorder")]
        public string workorder { get; set; }

        [XmlElement(ElementName = "fg_warehouse")]
        public string fg_warehouse { get; set; }

        [XmlElement(ElementName = "Finish_item")]
        public Finish_item Finish_item { get; set; }

        [XmlElement(ElementName = "Scrap")]
        public Scrap Scrap { get; set; }

        [XmlElement(ElementName = "Component")]
        public List<Component> Component { get; set; }
    }

    [XmlRoot(ElementName = "WorkOrder_UPD")]
    public class WorkOrder_UPD
    {

        [XmlElement(ElementName = "WoHdr_Rec")]
        public WoHdr_Rec WoHdr_Rec { get; set; }
    }
}
