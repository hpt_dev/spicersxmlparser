﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parser.Models
{
    public class ParseResults
    {
        public string Message { get; set; }
        public bool Pass { get; set; }
        public string ext_workorder { get; set; }
        public string workorder { get; set; }

        public ParseResults(string message, bool pass)
        {
            Message = message;
            Pass = pass;
        }
    }
}
