﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parser.Models
{
    public class LogParams
    {
        public string Start { get; set; }
        public string End { get; set; }
    }
}
