﻿using Microsoft.EntityFrameworkCore.Internal;
using parser.Models.Log;
using parser.Models.OutgoingXML;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using WinSCP;

namespace parser.Models
{
    public class XMLGenerator
    {
        public string DropDirectory { get; set; } = "";
        public string FTPDropDirectory { get; set; } = "";
        public string FailDirectory { get; set; } = "";
        public string PassDirectory { get; set; } = "";
        public SessionOptions FTPSessionOptions { get; set; }

        public FileSystemWatcher fileSystemWatcher;

        public bool Init()
        {
            GetDirectory();
            return true;
        }

        public bool Init(string directory)
        {
            DropDirectory = directory;
            FailDirectory = DropDirectory + "\\failed\\";
            PassDirectory = DropDirectory + "\\passed\\";

            //TestConnection();
            return true;
        }

        public bool DropXML(XmlDocument document, string fileName)
        {
            document.Save(DropDirectory + "\\" + fileName);
            return true;
        }

        

        private bool WriteFileToDatabase(string filePath, string fileName, string status, string id, string uniqueID)
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("@fileName", fileName));
                parms.Add(new SqlParameter("@filePath", filePath));
                parms.Add(new SqlParameter("@status", status));
                parms.Add(new SqlParameter("@wo_master_id", id));
                parms.Add(new SqlParameter("@unique_id", uniqueID));

                XMLDatabase.sql.ExecuteStoredProcedure("dbo.spAddUpdate_OutgoingFile", parms);
                return true;
            }
            catch
            {

            }
            return false;
        }

        public bool GetData(string id, OutgoingTypes outgoingType)
        {
            //Get Data from Integration database based upon an ID
            XmlDocument doc = new XmlDocument();
            switch (outgoingType)
            {
                case OutgoingTypes.WorkOrderUpdate:
                    {
                        GetWorkOrderData(id);
                        break;
                    }
                default:
                    break;
            }

            return true;
        }

        private bool GetWorkOrderData(string id)
        {
            try
            {
                var data = new WorkOrder_UPD();
                DataTable results = PHXDatabase.GetWorkOrderDataForExport(Convert.ToInt64(id));
                string fileName = "";
                string phx_work_order = "";

                if (results.Rows.Count == 0)
                {
                    Logger.Log("No Order Found for Export");
                    return false;
                }

                foreach (DataRow row in results.Rows)
                {
                    switch (row["record_type"].ToString())
                    {
                        case "Header":
                            {
                                fileName = row["filename"] != null ? row["filename"].ToString() : "";
                                if (data.WoHdr_Rec == null)
                                {
                                    data.WoHdr_Rec = new WoHdr_Rec();
                                }

                                phx_work_order = row["ext_workorder"] != null ? row["ext_workorder"].ToString() : "";
                                data.WoHdr_Rec.workorder = row["ext_workorder"] != null ? row["ext_workorder"].ToString() : "";
                                data.WoHdr_Rec.wo_status = row["wo_status"] != null ? row["wo_status"].ToString() : "";
                                data.WoHdr_Rec.UniqueID = row["UniqueID"] != null ? row["UniqueID"].ToString() : "";
                                data.WoHdr_Rec.fg_warehouse = row["fg_warehouse"] != null ? row["fg_warehouse"].ToString() : "";
                                data.WoHdr_Rec.company = row["company"] != null ? row["company"].ToString() : "";

                                if (data.WoHdr_Rec.Finish_item == null)
                                {
                                    data.WoHdr_Rec.Finish_item = new Finish_item();
                                }

                                data.WoHdr_Rec.Finish_item.parent_item = row["parent_item"] != null ? row["parent_item"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.parent_description = row["parent_item_description"] != null ? row["parent_item_description"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.Item_Qty = row["Item_Qty"] != null ? row["Item_Qty"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.LB = row["Finish_LB"] != null ? row["Finish_LB"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.work_center = row["work_center"] != null ? row["work_center"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.Run_Labr_hrs = row["Run_Labr_hrs"] != null ? row["Run_Labr_hrs"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.Run_Labr_Rate = row["Run_Labr_Rate"] != null ? row["Run_Labr_Rate"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.Run_Labr_units = row["Run_Labr_units"] != null ? row["Run_Labr_units"].ToString() : "";
                                data.WoHdr_Rec.Finish_item.Unit_of_Measure = row["Unit_of_Measure"] != null ? row["Unit_of_Measure"].ToString() : "";
                                break;
                            }
                        case "Production":
                            {
                                if (data.WoHdr_Rec.Finish_item == null)
                                {
                                    data.WoHdr_Rec.Finish_item = new Finish_item();
                                }

                                if (data.WoHdr_Rec.Finish_item.Inventory == null)
                                {
                                    data.WoHdr_Rec.Finish_item.Inventory = new ProductionInventory();
                                    
                                }
                                
                                //COPRODUCTS
                                if (row["prod_coproduct_order"].ToString() != "-" && row["prod_coproduct_order"] != null)
                                {
                                    if (data.WoHdr_Rec.Finish_item.Coproducts == null)
                                    {
                                        data.WoHdr_Rec.Finish_item.Coproducts = new Coproducts();
                                        data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory = new List<CFinish_Inventory>();

                                    }

                                    CFinish_Inventory cFinish_Inventory = new CFinish_Inventory();
                                    cFinish_Inventory.Coproduct_seq = row["prod_coproduct_order"] != null ? row["prod_coproduct_order"].ToString() : "";
                                    cFinish_Inventory.Coproduct_item = row["prod_coproduct_item"] != null ? row["prod_coproduct_item"].ToString() : "";
                                    cFinish_Inventory.CFinish_Location = new List<CFinish_Location>();

                                    if (data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory.Count() == 0)
                                    {
                                        data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory.Add(cFinish_Inventory);
                                    }

                                    int indexOfCoproduct = -1;
                                    for (int i = 0; i < data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory.Count(); i++)
                                    {
                                        if (row["prod_coproduct_order"].ToString() == data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory[i].Coproduct_seq.ToString())
                                        {
                                            indexOfCoproduct = i;
                                            break;
                                        }
                                    }
                                    if (indexOfCoproduct == -1)
                                    {
                                        data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory.Add(cFinish_Inventory);
                                        indexOfCoproduct = data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory.Count() - 1;
                                    }

                                    CFinish_Location cFinish_Location = new CFinish_Location();
                                    cFinish_Location.CFinish_lot_no = row["prod_inventory_lotno"] != null ? row["prod_inventory_lotno"].ToString() : "";
                                    cFinish_Location.CFinish_Qty_Oh = row["prod_inventory_qty"] != null ? row["prod_inventory_qty"].ToString() : "";
                                    cFinish_Location.CFinish_Qty_UOM = row["prod_inventory_uom"] != null ? row["prod_inventory_uom"].ToString() : "";
                                    cFinish_Location.CFinish_Qty_Oh_2 = row["prod_inventory_qty_2"] != null ? row["prod_inventory_qty_2"].ToString() : "";
                                    cFinish_Location.CFinish_Qty_UOM_2 = row["prod_inventory_uom_2"] != null ? row["prod_inventory_uom_2"].ToString() : "";
                                    cFinish_Location.CFinish_mweight = row["prod_inventory_mweight"] != null ? row["prod_inventory_mweight"].ToString() : "";
                                    cFinish_Location.CFinish_assign_seq = row["prod_inventory_assignseq"] != null ? row["prod_inventory_assignseq"].ToString() : "";

                                    data.WoHdr_Rec.Finish_item.Coproducts.CFinish_Inventory[indexOfCoproduct].CFinish_Location.Add(cFinish_Location);

                                }
                                else // SIMPLE ORDERS
                                {

                                    if (data.WoHdr_Rec.Finish_item.Inventory.Location == null)
                                    {
                                        data.WoHdr_Rec.Finish_item.Inventory.Location = new List<ProductionLocation>();
                                    }

                                    ProductionLocation prodLocation = new ProductionLocation();
                                    prodLocation.lot_no = row["prod_inventory_lotno"] != null ? row["prod_inventory_lotno"].ToString() : "";
                                    prodLocation.Qty_Oh = row["prod_inventory_qty"] != null ? row["prod_inventory_qty"].ToString() : "";
                                    prodLocation.Qty_UOM = row["prod_inventory_uom"] != null ? row["prod_inventory_uom"].ToString() : "";
                                    prodLocation.Qty_Oh_2 = row["prod_inventory_qty_2"] != null ? row["prod_inventory_qty_2"].ToString() : "";
                                    prodLocation.Qty_UOM_2 = row["prod_inventory_uom_2"] != null ? row["prod_inventory_uom_2"].ToString() : "";
                                    prodLocation.mweight = row["prod_inventory_mweight"] != null ? row["prod_inventory_mweight"].ToString() : "";
                                    prodLocation.assign_seq = row["prod_inventory_assignseq"] != null ? row["prod_inventory_assignseq"].ToString() : "";

                                    data.WoHdr_Rec.Finish_item.Inventory.Location.Add(prodLocation);
                                }

                                
                                
                                break;
                            }
                        case "Component":
                            {
                                bool addComponent = false;
                                if (data.WoHdr_Rec.Component == null)
                                {
                                    data.WoHdr_Rec.Component = new List<Component>();
                                }

                                Component component = data.WoHdr_Rec.Component.FirstOrDefault<Component>(x => x.item == (row["raw_item"] != null ? row["raw_item"].ToString() : ""));
                                if (component == null)
                                {
                                    component = new Component()
                                    {
                                        RawInventory = new RawInventory()
                                        {
                                            Location = new List<RawLocation>()
                                        },
                                        item = row["raw_item"] != null ? row["raw_item"].ToString() : "",
                                        item_description = row["raw_item_description"] != null ? row["raw_item_description"].ToString() : "",
                                        Component_warehouse = row["raw_inventory_warehouse"] != null ? row["raw_inventory_warehouse"].ToString() : ""
                                    };

                                    addComponent = true;
                                }

                                RawLocation rawLocation = new RawLocation();
                                rawLocation.location = row["raw_inventory_location"] != null ? row["raw_inventory_location"].ToString() : "";
                                rawLocation.lot_no = row["raw_inventory_lotno"] != null ? row["raw_inventory_lotno"].ToString() : "";
                                rawLocation.Qty_Oh = row["raw_inventory_qty"] != null ? row["raw_inventory_qty"].ToString() : "";
                                rawLocation.uom = row["raw_inventory_uom"] != null ? row["raw_inventory_uom"].ToString() : "";

                                //rawLocation.mweight = row["raw_inventory_mweight"] != null ? row["raw_inventory_mweight"].ToString() : "";
                                rawLocation.assign_seq = row["raw_inventory_assign_seq"] != null ? row["raw_inventory_assign_seq"].ToString() : "";

                                component.RawInventory.Location.Add(rawLocation);
                                if (addComponent)
                                    data.WoHdr_Rec.Component.Add(component);
                                break;
                            }
                        case "Scrap":
                            {
                                if (data.WoHdr_Rec.Scrap == null)
                                {
                                    data.WoHdr_Rec.Scrap = new Scrap();
                                }
                                data.WoHdr_Rec.Scrap.Scrap_Item = row["Scrap_Item"] != null ? row["Scrap_Item"].ToString() : "";
                                data.WoHdr_Rec.Scrap.Scrap_Qty = row["Scrap_Qty"] != null ? row["Scrap_Qty"].ToString() : "";
                                data.WoHdr_Rec.Scrap.UOM = row["UOM"] != null ? row["UOM"].ToString() : "";
                                break;
                            }
                        default:
                            break;
                    }

                    //Check For Empty Scrap
                    if (data.WoHdr_Rec.Scrap == null)
                    {
                        if (data.WoHdr_Rec.Scrap == null)
                        {
                            data.WoHdr_Rec.Scrap = new Scrap();
                        }
                        data.WoHdr_Rec.Scrap.Scrap_Item = "";
                        data.WoHdr_Rec.Scrap.Scrap_Qty = "";
                        data.WoHdr_Rec.Scrap.UOM = "";
                    }
                }

                Logger.Log("Exporting Order [A+]: " + data.WoHdr_Rec.workorder.ToString());

               // XmlSerializer serializer = new XmlSerializer(typeof(WorkOrder_UPD));

                string filePath = DropDirectory + "\\" + fileName;
                //Stream fs = new FileStream(DropDirectory + "\\" + fileName, FileMode.Create);
                //var ns = new XmlSerializerNamespaces();
                //ns.Add("", "");
                //XmlWriterSettings settings = new XmlWriterSettings
                //{
                //    OmitXmlDeclaration = true,
                //    ConformanceLevel = ConformanceLevel.Fragment,
                //    CloseOutput = true,
                //};


                //XmlWriter writer = XmlWriter.Create(fs, settings);

                //serializer.Serialize(writer, data, ns);

                //XmlSerializerNamespaces ns = new XmlSerializerNamespaces(); 
                //ns.Add("", "");
                //using (XmlWriter writer = XmlWriter.Create(fs, settings))
                //{
                //    new XmlSerializer(typeof(WorkOrder_UPD)).Serialize(writer, data, ns);
                //}

                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };

                // Remove Namespace  
                var ns = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

                var xmlData = "";
                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    var serializer = new XmlSerializer(typeof(WorkOrder_UPD));
                    serializer.Serialize(writer, data, ns);
                    xmlData = stream.ToString();
                }

                //fs.Close();

                Console.WriteLine(xmlData);
                Console.WriteLine(DropDirectory + "\\" + fileName);

                File.WriteAllText(DropDirectory + "\\" + fileName, xmlData);           


                Logger.Log("Wrote local XML - " + filePath.ToString());

                if (TestFTPConnection(FTPSessionOptions))
                {
                    using (Session session = new Session())
                    {
                        // Connect
                        session.Open(FTPSessionOptions);
                        Logger.Log("Connected to FTP");

                        session.PutFileToDirectory(DropDirectory + "\\" + fileName, FTPDropDirectory);
                        Logger.Log("Created File on FTP");
                    }
                }
                WriteFileToDatabase(filePath, fileName, "passed", id, data.WoHdr_Rec.workorder.ToString());
                XMLDatabase.SaveXMLFileToDatabase(GetFileNameFromPath(filePath), xmlData, "IN");

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Log(ex.Message);
            }

            return false;
        }

        private string GetFileNameFromPath(string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf("\\") + 1, filePath.Length - filePath.LastIndexOf("\\") - 1);
        }

        private bool TestFTPConnection(SessionOptions options)
        {
            Session testSession = new Session();
            try
            {
                Logger.Log("Testing FTP Connection");
                testSession.Open(options);
                testSession.Close();

                return true;
            }
            catch
            {
                Logger.Log("FTP Failed to Connect");

                return false;
            }
        }

        public bool UpdateDirectory(string parseDirectory)
        {
            try
            {
                DropDirectory = parseDirectory;

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("@parse", parseDirectory));

                XMLDatabase.sql.ExecuteStoredProcedure("dbo.spUpdate_OutgoingDirectories", parms);

                return true;
            }
            catch (Exception)
            {


            }

            return false;
        }

        public bool GetDirectory()
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();


                DataTable dtResults = XMLDatabase.sql.ExecuteStoredProcedure("dbo.spGet_Directories", parms);
                DropDirectory = dtResults.Rows[0]["outgoing_drop"].ToString();
                FTPDropDirectory = dtResults.Rows[0]["ftp_outgoing_directory"].ToString();

                FTPSessionOptions = new SessionOptions()
                {
                    Protocol = Protocol.Ftp,
                    HostName = dtResults.Rows[0]["ftp_host"].ToString(),
                    UserName = dtResults.Rows[0]["ftp_username"].ToString(),
                    Password = dtResults.Rows[0]["ftp_password"].ToString(),
                    PortNumber = Convert.ToInt32(dtResults.Rows[0]["ftp_port"])
                };
                return true;
            }
            catch (Exception)
            {


            }

            return false;
        }
    }

    public class XmlFragmentWriter : XmlTextWriter
    {
        // Add whichever constructor(s) you need, e.g.:
        public XmlFragmentWriter(Stream stream, Encoding encoding) : base(stream, encoding)
        {
        }

        public override void WriteStartDocument()
        {
            // Do nothing (omit the declaration)
        }
    }
}
