﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using parser.Models.IncomingXML;
using System.Xml;
using parser.Models.Log;

namespace parser.Models
{
    public static class XMLDatabase
    {
        //private static string ConnString = "Data Source=localhost;Initial Catalog=xmlparse;Persist Security Info=True;User ID=xml_user;Password=xml";
        private static string ConnString = "";//"Data Source=25.95.248.242;Initial Catalog=xmlparse;Persist Security Info=True;User ID=xml_user;Password=xml";
        public static Database sql = new Database(ConnString);

        #region Stored Procedure names
        public const string InsertLogMessage = "qb.InsertLogMessage";
        public const string InsertMessageQueueItem = "qb.InsertMessageQueueItem";
        public const string spSaveXMLFile = "spSaveXMLFile";
        #endregion

        public static void SaveXMLFileToDatabase(string fileName, string xml, string incomingOrOutgoing)
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("@fileName", fileName));
                parms.Add(new SqlParameter("@xmlFile", xml));
                parms.Add(new SqlParameter("@incomingOrOutgoing", incomingOrOutgoing));

                XMLDatabase.sql.ExecuteStoredProcedure(spSaveXMLFile, parms);
            }
            catch (Exception ex)
            {
                Logger.Log("Failed to Save XML file to Database");
                Logger.Log(ex.Message);
                
            }
        }
    }
}