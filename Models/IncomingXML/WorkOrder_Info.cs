﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;
using System.Threading.Tasks;

namespace parser.Models.IncomingXML
{
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(WorkOrder_Info));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (WorkOrder_Info)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "Bill_to_Customer")]
    public class Bill_to_Customer
    {

        [XmlElement(ElementName = "customerNum")]
        public string customerNum { get; set; }

        [XmlElement(ElementName = "Customer_name")]
        public string Customer_name { get; set; }

        [XmlElement(ElementName = "Customer_Address_1")]
        public string Customer_Address_1 { get; set; }

        [XmlElement(ElementName = "Customer_Address_2")]
        public string Customer_Address_2 { get; set; }

        [XmlElement(ElementName = "Customer_Address_3")]
        public string Customer_Address_3 { get; set; }

        [XmlElement(ElementName = "Customer_City")]
        public string Customer_City { get; set; }

        [XmlElement(ElementName = "Customer_State")]
        public string Customer_State { get; set; }

        [XmlElement(ElementName = "Customer_Zip")]
        public string Customer_Zip { get; set; }

        [XmlElement(ElementName = "Customer_Attention_to")]
        public string Customer_Attention_to { get; set; }
    }

    [XmlRoot(ElementName = "Ship_to_Customer")]
    public class Ship_to_Customer
    {

        [XmlElement(ElementName = "ShipTo_Number")]
        public string ShipTo_Number { get; set; }

        [XmlElement(ElementName = "ShipTo_Name")]
        public string ShipTo_Name { get; set; }

        [XmlElement(ElementName = "ShipTo_Address_1")]
        public string ShipTo_Address_1 { get; set; }

        [XmlElement(ElementName = "ShipTo_Address_2")]
        public string ShipTo_Address_2 { get; set; }

        [XmlElement(ElementName = "ShipTo_Address_3")]
        public string ShipTo_Address_3 { get; set; }

        [XmlElement(ElementName = "ShipTo_City")]
        public string ShipTo_City { get; set; }

        [XmlElement(ElementName = "ShipTo_State")]
        public string ShipTo_State { get; set; }

        [XmlElement(ElementName = "ShipTo_ZipCode")]
        public string ShipTo_ZipCode { get; set; }

        [XmlElement(ElementName = "ShipTo_Attention_to")]
        public string ShipTo_Attention_to { get; set; }
    }

    [XmlRoot(ElementName = "fg_warehouse")]
    public class fg_warehouse
    {

        [XmlElement(ElementName = "fg_warehouse")]
        public string fgwarehouse { get; set; }

        [XmlElement(ElementName = "FgWarehouse_description")]
        public string FgWarehouse_description { get; set; }

        [XmlElement(ElementName = "FgWarehouse_Address1")]
        public string FgWarehouse_Address1 { get; set; }

        [XmlElement(ElementName = "FgWarehouse_Address2")]
        public string FgWarehouse_Address2 { get; set; }

        [XmlElement(ElementName = "FgWarehouse_City")]
        public string FgWarehouse_City { get; set; }

        [XmlElement(ElementName = "FgWarehouse_State")]
        public string FgWarehouse_State { get; set; }

        [XmlElement(ElementName = "FgWarehouse_ZipCode")]
        public string FgWarehouse_ZipCode { get; set; }

        [XmlElement(ElementName = "FgWarehouse_PhoneNumber")]
        public string FgWarehouse_PhoneNumber { get; set; }
    }

    [XmlRoot(ElementName = "Finish_item")]
    public class Finish_item
    {

        [XmlElement(ElementName = "parent_item")]
        public string parent_item { get; set; }

        [XmlElement(ElementName = "parent_description")]
        public string parent_description { get; set; }

        [XmlElement(ElementName = "mweight")]
        public string mweight { get; set; }

        [XmlElement(ElementName = "FinishGrade")]
        public string FinishGrade { get; set; }

        [XmlElement(ElementName = "FinishBasisCaliper")]
        public string FinishBasisCaliper { get; set; }

        [XmlElement(ElementName = "FinishWidth")]
        public string FinishWidth { get; set; }

        [XmlElement(ElementName = "Finish_WidthAlpha")]
        public string Finish_WidthAlpha { get; set; }

        [XmlElement(ElementName = "Finish_LengthAlpha")]
        public string Finish_LengthAlpha { get; set; }

        [XmlElement(ElementName = "SFI_Description")]
        public string SFI_Description { get; set; }

        [XmlElement(ElementName = "Finish_DimensionAlpha")]
        public string Finish_DimensionAlpha { get; set; }

        [XmlElement(ElementName = "Finish_widthNumeric")]
        public string Finish_widthNumeric { get; set; }

        [XmlElement(ElementName = "Finish_lengthNumeric")]
        public string Finish_lengthNumeric { get; set; }

        [XmlElement(ElementName = "Finish_dimensionNumeric")]
        public string Finish_dimensionNumeric { get; set; }

        [XmlElement(ElementName = "Finish_item_size")]
        public string Finish_item_size { get; set; }

        [XmlElement(ElementName = "cust_order_qty")]
        public string cust_order_qty { get; set; }

        [XmlElement(ElementName = "unit_of_measure")]
        public string unit_of_measure { get; set; }

        [XmlElement(ElementName = "productDescriptionLabel")]
        public string productDescriptionLabel { get; set; }

        [XmlElement(ElementName = "Finish_PAL_UOM")]
        public string Finish_PAL_UOM { get; set; }

        [XmlElement(ElementName = "Finish_PAL_Qty")]
        public string Finish_PAL_Qty { get; set; }
    }

    [XmlRoot(ElementName = "Coproducts")]
    public class Coproducts
    {

        [XmlElement(ElementName = "Coproduct_seq")]
        public List<Coproduct_seq> Coproduct_seq { get; set; }
    }

    [XmlRoot(ElementName = "coproduct_seq")]
    public class Coproduct_seq
    {
        [XmlElement(ElementName = "CoproductR_seq")]
        public string CoproductR_seq { get; set; }

        [XmlElement(ElementName = "Coproduct_item")]
        public string Coproduct_item { get; set; }

        [XmlElement(ElementName = "Coproduct_description")]
        public string Coproduct_description { get; set; }

        [XmlElement(ElementName = "Coproduct_SFI_Description")]
        public string Coproduct_SFI_Description { get; set; }

        [XmlElement(ElementName = "Coproduct_mweight")]
        public string Coproduct_mweight { get; set; }

        [XmlElement(ElementName = "Coproduct_Grade")]
        public string Coproduct_Grade { get; set; }

        [XmlElement(ElementName = "Coproduct_BasisCaliper")]
        public string Coproduct_BasisCaliper { get; set; }

        [XmlElement(ElementName = "Coproduct_WidthAlpha")]
        public string Coproduct_WidthAlpha { get; set; }

        [XmlElement(ElementName = "Coproduct_LengthAlpha")]
        public string Coproduct_LengthAlpha { get; set; }

        [XmlElement(ElementName = "Coproduct_DimensionAlpha")]
        public string Coproduct_DimensionAlpha { get; set; }

        [XmlElement(ElementName = "Coproduct_widthNumeric")]
        public string Coproduct_widthNumeric { get; set; }

        [XmlElement(ElementName = "Coproduct_lengthNumeric")]
        public string Coproduct_lengthNumeric { get; set; }

        [XmlElement(ElementName = "Coproduct_dimensionNumeric")]
        public string Coproduct_dimensionNumeric { get; set; }

        [XmlElement(ElementName = "Coproduct_item_size")]
        public string Coproduct_item_size { get; set; }

        [XmlElement(ElementName = "Coproduct_order_qty")]
        public string Coproduct_order_qty { get; set; }

        [XmlElement(ElementName = "Coproduct_UOM")]
        public string Coproduct_UOM { get; set; }

        [XmlElement(ElementName = "Coproduct_PAL_Qty")]
        public string Coproduct_PAL_Qty { get; set; }

        [XmlElement(ElementName = "Coproduct_PAL_UOM")]
        public string Coproduct_PAL_UOM { get; set; }

        [XmlElement(ElementName = "Coproduct_DescriptionLabel")]
        public string Coproduct_DescriptionLabel { get; set; }

        [XmlElement(ElementName = "Coproduct_Layout")]
        public string Coproduct_Layout { get; set; }
    }

    [XmlRoot(ElementName = "Routing")]
    public class Routing
    {

        [XmlElement(ElementName = "routing_seq")]
        public string routing_seq { get; set; }

        [XmlElement(ElementName = "department")]
        public string department { get; set; }

        [XmlElement(ElementName = "department_desc")]
        public string department_desc { get; set; }

        [XmlElement(ElementName = "work_center")]
        public string work_center { get; set; }

        [XmlElement(ElementName = "work_center_desc")]
        public string work_center_desc { get; set; }

        [XmlElement(ElementName = "operation")]
        public string operation { get; set; }

        [XmlElement(ElementName = "operation_desc")]
        public string operation_desc { get; set; }

        [XmlElement(ElementName = "std_rate")]
        public string std_rate { get; set; }
    }

    [XmlRoot(ElementName = "Location")]
    public class Location
    {

        [XmlElement(ElementName = "assign_seq")]
        public string assign_seq { get; set; }

        [XmlElement(ElementName = "location")]
        public string location { get; set; }

        [XmlElement(ElementName = "lot_no")]
        public string lot_no { get; set; }

        [XmlElement(ElementName = "Qty_Oh")]
        public string Qty_Oh { get; set; }

        [XmlElement(ElementName = "uom")]
        public string uom { get; set; }

        [XmlElement(ElementName = "mweight")]
        public string mweight { get; set; }

        [XmlElement(ElementName = "linealFeet")]
        public string linealFeet { get; set; }
    }

    [XmlRoot(ElementName = "Inventory")]
    public class Inventory
    {

        [XmlElement(ElementName = "Location")]
        public List<Location> Location { get; set; }
    }

    [XmlRoot(ElementName = "Components")]
    public class Components
    {
        [XmlElement(ElementName = "Component")]
        public List<Component> Component { get; set; }
    }
    
    [XmlRoot(ElementName = "Component")]
    public class Component
    {

        [XmlElement(ElementName = "component_warehouse")]
        public string component_warehouse { get; set; }

        [XmlElement(ElementName = "CmWarehouse_description")]
        public string CmWarehouse_description { get; set; }

        [XmlElement(ElementName = "CmWarehouse_Address1")]
        public string CmWarehouse_Address1 { get; set; }

        [XmlElement(ElementName = "CmWarehouse_Address2")]
        public string CmWarehouse_Address2 { get; set; }

        [XmlElement(ElementName = "CmWarehouse_City")]
        public string CmWarehouse_City { get; set; }

        [XmlElement(ElementName = "CmWarehouse_State")]
        public string CmWarehouse_State { get; set; }

        [XmlElement(ElementName = "CmWarehouse_ZipCode")]
        public string CmWarehouse_ZipCode { get; set; }

        [XmlElement(ElementName = "CmWarehouse_PhoneNumber")]
        public string CmWarehouse_PhoneNumber { get; set; }

        [XmlElement(ElementName = "comp_item")]
        public string comp_item { get; set; }

        [XmlElement(ElementName = "item_desc")]
        public string item_desc { get; set; }

        [XmlElement(ElementName = "Cm_Mweight")]
        public string Cm_Mweight { get; set; }

        [XmlElement(ElementName = "Cmitem_WidthAlpha")]
        public string Cmitem_WidthAlpha { get; set; }

        [XmlElement(ElementName = "Cmitem_widthNumeric")]
        public string Cmitem_widthNumeric { get; set; }

        [XmlElement(ElementName = "Cmitem_lengthNumeric")]
        public string Cmitem_lengthNumeric { get; set; }

        [XmlElement(ElementName = "Cmitem_length")]
        public string Cmitem_length { get; set; }

        [XmlElement(ElementName = "Cmitem_size")]
        public string Cmitem_size { get; set; }

        [XmlElement(ElementName = "Comp_RollSize")]
        public string Comp_RollSize { get; set; }

        [XmlElement(ElementName = "qty_order")]
        public string qty_order { get; set; }

        [XmlElement(ElementName = "qty_commited")]
        public string qty_commited { get; set; }

        [XmlElement(ElementName = "Inventory")]
        public Inventory Inventory { get; set; }
    }

    [XmlRoot(ElementName = "WoHdr_Rec")]
    public class WoHdr_Rec
    {

        [XmlElement(ElementName = "wo_status")]
        public string wo_status { get; set; }

        [XmlElement(ElementName = "wo_type")]
        public string wo_type { get; set; }

        [XmlElement(ElementName = "UniqueID")]
        public string UniqueID { get; set; }

        [XmlElement(ElementName = "company")]
        public string company { get; set; }

        [XmlElement(ElementName = "workorder")]
        public string workorder { get; set; }

        [XmlElement(ElementName = "customer_order")]
        public string customer_order { get; set; }

        [XmlElement(ElementName = "customerNum")]
        public string customerNum { get; set; }

        [XmlElement(ElementName = "po_Customer_no")]
        public string po_Customer_no { get; set; }

        [XmlElement(ElementName = "Job_Number")]
        public string Job_Number { get; set; }

        [XmlElement(ElementName = "EntryDate")]
        public string EntryDate { get; set; }

        [XmlElement(ElementName = "DueDate")]
        public string DueDate { get; set; }

        [XmlElement(ElementName = "Bill_to_Customer")]
        public Bill_to_Customer Bill_to_Customer { get; set; }

        [XmlElement(ElementName = "Ship_to_Customer")]
        public Ship_to_Customer Ship_to_Customer { get; set; }

        [XmlElement(ElementName = "fg_warehouse")]
        public fg_warehouse fg_warehouse { get; set; }

        [XmlElement(ElementName = "Finish_item")]
        public Finish_item Finish_item { get; set; }

        [XmlElement(ElementName = "Routing")]
        public Routing Routing { get; set; }

        [XmlElement(ElementName = "Coproducts")]
        public Coproducts Coproduct { get; set; }

        [XmlElement(ElementName = "Components")]
        public Components Components { get; set; }
    }

    [XmlRoot(ElementName = "WorkOrder_Info")]
    public class WorkOrder_Info
    {

        [XmlElement(ElementName = "WoHdr_Rec")]
        public WoHdr_Rec WoHdr_Rec { get; set; }
    }


}
