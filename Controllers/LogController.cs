﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using parser.Services;
using parser.Models;
using System.Data;
using Microsoft.Extensions.Options;

namespace parser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        public LogController(IOptions<ConfigSettings> config)
        {
            Console.Out.Write("LogController");
            if (PHXDatabase.sql.ConnectionString == null)
            {
                PHXDatabase.sql.ConnectionString = config.Value.PHXConnectionString;
            }
            if (XMLDatabase.sql.ConnectionString == null)
            {
                XMLDatabase.sql.ConnectionString = config.Value.XMLConnectionString;
            }
        }

        // GET api/getall_incomingfiles
        [HttpPost, Route("getall_incomingfiles")]
        public ActionResult GetAllFiles_Incoming([FromBody] LogParams value)
        {
            string sql = "exec spGet_IncomingFiles @status = '%', @start = '" + value.Start  + "', @end = '" + value.End + "'";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }

        // GET api/getpassed_incomingfiles
        [HttpPost, Route("getpassed_incomingfiles")]
        public ActionResult GetPassedFiles_Incoming()
        {
            string sql = "exec spGet_IncomingFiles @status = 'Pass', @start = '" + DateTime.Now.AddDays(-7).ToString() + "', @end = '" + DateTime.Now.ToString() + "'";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }

        // GET api/getfailed_incomingfiles
        [HttpPost, Route("getfailed_incomingfiles")]
        public ActionResult GetFailedFiles_Incoming()
        {
            string sql = "exec spGet_IncomingFiles @status = 'Fail', @start = '" + DateTime.Now.AddDays(-7).ToString() + "', @end = '" + DateTime.Now.ToString() + "'";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }

        // POST api/getall_outgoingfiles
        [HttpPost, Route("getall_outgoingfiles")]
        public ActionResult GetAllFiles_Outgoing([FromBody] LogParams value)
        {
            string sql = "exec spGet_OutgoingFiles @status = '%', @start = '" + value.Start + "', @end = '" + value.End + "'";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }

        // POST api/getpassed_outgoingfiles
        [HttpPost, Route("getpassed_outgoingfiles")]
        public ActionResult GetPassedFiles_Outgoing([FromBody] string value)
        {
            string sql = "exec spGet_OutgoingFiles @status = 'Pass', @start = '" + DateTime.Now.AddDays(-7).ToString() + "', @end = '" + DateTime.Now.ToString() + "'";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }

        // POST api/getfailed_outgoingfiles
        [HttpPost, Route("getfailed_outgoingfiles")]
        public ActionResult GetFailedFiles_Outgoing()
        {
            string sql = "exec spGet_OutgoingFiles @status = 'Fail', @start = '" + DateTime.Now.AddDays(-7).ToString() + "', @end = '" + DateTime.Now.ToString() + "'";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }

        // POST api/getlog
        [HttpPost, Route("getlog")]
        public ActionResult GetLog([FromBody] LogParams value)
        {
            string sql = "SELECT create_date, message from logs WHERE create_date between '" + value.Start + "' and '" + value.End + "'  order by ID desc";
            return new JsonResult(XMLDatabase.sql.ExecuteQueryToList(sql));
        }
    }
}
