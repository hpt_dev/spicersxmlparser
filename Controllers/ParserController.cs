﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using parser.Models;
using parser.Services;

namespace parser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParserController : ControllerBase
    {
        public ParserController(IOptions<ConfigSettings> config)
        {
            Console.Out.Write("ParserController");
            if (PHXDatabase.sql.ConnectionString == null)
            {
                PHXDatabase.sql.ConnectionString = config.Value.PHXConnectionString;
            }
            if (XMLDatabase.sql.ConnectionString == null)
            {
                XMLDatabase.sql.ConnectionString = config.Value.XMLConnectionString;
            }

            XMLService.Init();
        }

        // POST api/change_in_directory
        [HttpPost, Route("change_in_directory")]
        public ActionResult<bool> ChangeIncomingDirectory([FromBody] string value)
        {
            XMLService.SetIncomingDirectory(value);
            return true;
        }

        // POST api/change_out_directory
        [HttpPost, Route("change_out_directory")]
        public ActionResult<bool> ChangeOutgoingDirectory([FromBody] string value)
        {
            XMLService.SetOutgoingDirectory(value);
            return true;
        }

        // POST api/startservice
        [HttpPost, Route("startservice")]
        public ActionResult<bool> StartService()
        {
            XMLService.StartParser();
            return true;
        }

        // POST api/stopservice
        [HttpPost, Route("stopservice")]
        public ActionResult<bool> StopService()
        {
            XMLService.StopParser();
            return true;
        }

        // POST api/stopservice
        [HttpPost, Route("parsefile")]
        public ActionResult<bool> ParseFile([FromBody] string filePath)
        {
            //Look up file in database
            //Most likely the file will be in the failed folder

            //Send file to parser
            XMLService.ParseFile(filePath);
            return true;
        }

        // POST api/createoutgoing
        [HttpPost, Route("createoutgoing")]
        public ActionResult<bool> CreateOutgoing([FromBody] string value)
        {
            XMLService.GenerateFile(value);
            return true;
        }
    }
}
