﻿using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

using parser.Services;
using Microsoft.IdentityModel.Protocols;
using System;

namespace parser
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var parse = new Thread(() =>
            {
                Console.Out.Write("TESTING");
                Thread.Sleep(30000);
                XMLService.Init();

                ////Start Parser
                XMLService.StartParser();
            });
            parse.IsBackground = true;
            parse.Start();

            //Start API
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
